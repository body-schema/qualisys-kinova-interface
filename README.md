# Teleoperation of Kinova3 Robot using Qualisys Motion Cameras

This project provides teleoperation capabilities for the Kinova3 robot using Qualisys motion cameras for tracking human motion.

**Note**: The Python version of this project is deprecated due to latency limitations. Please refer to the C++ version for an up-to-date implementation.


### Installing

1. Clone the repository onto your local machine:
```git clone https://gitlab.fel.cvut.cz/body-schema/qualisys-kinova-interface.git```

2. Install required dependencies
   - For CPP code, refer to the README file in the `cpp/` folder.
   - For Python code (deprecated), refer to the README file in the `python/` folder.

## Folder Structure

The project has the following folder structure:

- `cpp/`: Contains the C++ code for teleoperation.
- `python/`: Contains the deprecated Python code.


## Built With

* [Kortex API](https://github.com/Kinovarobotics/kortex) - Used for communication with the Kinova3 robot
* [Qualisys Motion Capture](https://www.qualisys.com/) - Used for capturing 3D human motion

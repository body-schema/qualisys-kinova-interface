#ifndef IK_COMPUTER_H
#define IK_COMPUTER_H

#pragma once

#include <iostream>
#include "Eigen/Dense"
#include <math.h>

#include "Robot.hpp"
#include "IKOptions.hpp"
#include "IK.hpp"

using namespace std;
using namespace Eigen;

// DH parameters for Kinova Gen3
const Robot<7> R(
	// Given as DOFx4 table, in the following order: a_i, alpha_i, d_i, theta_i.
	(Matrix<double, 7, 4>() <<
	 0,     M_PI/2,  	-0.2848,	0,
	 0,     M_PI/2,		-0.0118,	M_PI,
	 0,	    M_PI/2,  	-0.4208,	M_PI,
	 0,  	M_PI/2,  	-0.0128,	M_PI,
	 0,  	M_PI/2,  	-0.3143,	M_PI,
	 0,  	M_PI/2,  	0,		    M_PI,
	 0,  	M_PI,     	-0.1674,	M_PI).finished(),
					  
	// Second argument is a list of joint types
	// true is prismatic, false is revolute
	// Kinova Gen3 only has revolute joints
	(Vector<bool,7>() << false, false, false, false, false, false, false).finished(),

	// Third agument is a list of joint directions
	// Allows you to change the sign (direction) of
	// the joints.
	(Vector<double,7>(7) << 1, 1, 1, 1, 1, 1, 1 ).finished(),

	// Fourth and fifth arguments are the base and tool transforms, respectively
	(Matrix4d() <<
    1, 	0,  0,   0,
    0,  -1, 0,   0,
    0,  0,  -1,  0,
    0,  0,  0,   1
    ).finished(),
//	(Matrix4d() << // Rotation is computed, 0.120 is from docs
//    0.0675, -0.4431,  -0.8939,  	 0,
//    0.9947,	  0.0397, 	-.0948,  	 0,
//    0.0775,  -0.8956,  	0.4381,  0.120,
//    0,  0,  0,   1
//    ).finished()

	(Matrix4d() << // rot 0*x, -60*y, 90*z - https://www.andre-gaschler.com/rotationconverter/
    1.,	  0.,	.0,		0,
    0.,	  1.,	.0,		0,
    0.,   0,	1.,	0.120,
    0,  0,  0,   1
    ).finished()
);

// Define the IK options
const IKOptions opt = IKOptions( 	200, // max number of iterations
									0, // algorithm (0 = QuIK, 1 = Newton-Raphson, 2 = BFGS)
									1e-12, // Exit tolerance
									1e-14, // Minimum step tolerance
									0.05, // iteration-to-iteration improvement tolerance (0.05 = 5% relative improvement)
									10, // max consequitive gradient fails
									80, // Max gradient fails
									1e-10, // lambda2 (lambda^2, the damping parameter for DQuIK and DNR)
									0.34, // Max linear error step
									1, // Max angular error step
									1e-5, // Sigma value for armijo rule in BFGS line search
									0.5 // beta value for armijo rule in BFGS line search
								);


int N = 1; // Number of poses to generate
int DOF = R.dof;

class IKComputer {
	private:
		Matrix<double,Dynamic,4> Tn = Matrix<double,Dynamic,4>::Zero(N*4, 4); 	// 4N * 4 matrix of vertically stacked transforms to be solved.
								// This is just a convenient way of sending in an array of transforms.

	public:
		Matrix<double,7,Dynamic> Q_star = Matrix<double,7,Dynamic>::Zero(DOF, N);	// Solver's solution
		Matrix<double,6,Dynamic> e_star = Matrix<double,6,Dynamic>::Zero(6,N);	// Error at solver pose
		Vector<int,Dynamic> iter = Vector<int,Dynamic>::Zero(N),	// Store number of iterations of algorithm
							breakReason = Vector<int,Dynamic>::Zero(N);	// Store break out reason

		IKComputer(Matrix3d rotation) {
			setRotation(rotation);
			Tn(3,3) = 1.;
		}

		void setRotation(Matrix3d rotation) {
			for (int i=0; i<3; i++) {
				for (int j=0; j<3; j++) {
					Tn(i,j) = rotation(i,j);
				}
			}
		}

		bool ik(Matrix<double,7,Dynamic> initial_joint_angles, Vector3d xyz) {
			// Outputs:
			//   	- Matrix<double,DOF,N>& Qstar: [DOFxN] The solved joint angles
			//		- Matrix<double,6,N>&e: The pose errors at the final solution.
			//		- Vector<int,N> iter: The number of iterations the algorithm took.
			//		- Vector<int,N> breakReason: The reason the algorithm stopped.
			//               1 - Tolerance reached
			//               2 - Min step size reached
			//               3 - Max iterations reached
			//               4 - Grad fails reached
			
			Tn(0, 3) = xyz.x();
			Tn(1, 3) = xyz.y();
			Tn(2, 3) = xyz.z();

			IK(R, Tn, initial_joint_angles, opt, Q_star, e_star, iter, breakReason);

			for (int i=0; i<N;i++) {
				if (breakReason[i] > 2) return false;
			}
			return true;
		}

		Eigen::Vector3d fk(Matrix<double,7,1> Q) {
			Matrix4d T;
			R.FKn(Q, T);
			return Eigen::Vector3d(T(0,3), T(1,3), T(2,3));
		}

		Eigen::Matrix4d fk_full(Matrix<double,7,1> Q) {
			Matrix4d T;
			R.FKn(Q, T);
			return T;
		}
};

#endif // IK_COMPUTER_H
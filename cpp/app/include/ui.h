#ifndef UI_H
#define UI_H

#pragma once

#include <iostream>
#include <unistd.h>
#include <termios.h>
#include <fstream>
#include <vector>
#include <sstream>

#include "spdlog/spdlog.h"
#include "ini.h"
#include "shared_memory.h"

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6) {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return std::move(out).str();
}

char getch() {
    char buf = 0;
    struct termios old = {0};
    if (tcgetattr(0, &old) < 0)
        spdlog::error("UI - getch: tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        spdlog::error("UI - getch: tcsetattr ICANON");

    fd_set set;
    FD_ZERO(&set);
    FD_SET(STDIN_FILENO, &set);
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 500000; // wait for 500 milliseconds
    int result = select(STDIN_FILENO + 1, &set, NULL, NULL, &timeout);
    if (result == -1)
        spdlog::error("UI - getch: select()");
    else if (result != 0 && read(0, &buf, 1) < 0)
        spdlog::error("UI - getch: read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        spdlog::error("UI - getch: tcsetattr ~ICANON");
    return (buf);
}

class Ui {
    private:
        mINI::INIStructure& config;
        SharedDataCamera& shared_data_camera;
        std::mutex& shared_data_camera_mutex;
        SharedDataUi& shared_data_ui;
        std::mutex& shared_data_ui_mutex;

        std::mt19937 random_engine;
        std::vector<double> random_trails;
        double SCREEN_HEIGHT_CM;
        int TOTAL_TRAINING_TRIALS = 3;
        int DISTRIBUTION_SIZE = 3;
        int SETS_OF_VALID_TRIALS = 3;
        bool loaded_hand_distance = false;

        bool label_timestamp(std::string event_message) {
            std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
            if (!shared_data_ui.event_trigger) {
                shared_data_ui.event_message = event_message;
                shared_data_ui.event_trigger = true;
                spdlog::info("Ui.label_timestamp: Labeled recording timestamp with '{}'", event_message);
                return true;
            } else {
                shared_data_ui.event_message += ";" + event_message;
                shared_data_ui.event_trigger = true;
                spdlog::info("Ui.label_timestamp: Labeled recording timestamp with '{}'", event_message);
                return true;
            }
        };

    public:
        Ui(mINI::INIStructure& config,
                SharedDataCamera& shared_data_camera,
                std::mutex& shared_data_camera_mutex,
                SharedDataUi& shared_data_ui,
                std::mutex& shared_data_ui_mutex)
        : config(config),
        shared_data_camera(shared_data_camera),
        shared_data_camera_mutex(shared_data_camera_mutex),
        shared_data_ui(shared_data_ui),
        shared_data_ui_mutex(shared_data_ui_mutex) {
            std::random_device rd;  // Will be used to obtain a seed for the random number engine
            random_engine = std::mt19937(rd()); // Standard mersenne_twister_engine seeded with rd()
            random_trails.push_back(1.);

            try {
                SCREEN_HEIGHT_CM = std::stod(config["Gui"]["screen_height_cm"]);
            }  catch (std::exception& e) {
                spdlog::error("Ui.init: Unable to parse screen_height_cm from GUI settings: {}", e.what());
            }
        }

        void operator()(std::stop_token should_run) {
            while(!should_run.stop_requested()) {
                char input = getch();
                if (input != 0) {
                    std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                    if (shared_data_ui.control_input == 0) {
                        shared_data_ui.control_input = input;
                    }
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }
        }

        bool update() {
            // Read control input from all places (console, GUI)
            char input = 0;
            SCENE_TYPE scene;
            std::chrono::high_resolution_clock::time_point scene_change_timestamp;
            VARIANT_TYPE variant;
            std::chrono::high_resolution_clock::time_point variant_change_timestamp;

            int trial, total_trials;
            double distance_cm, max_distance_cm;

            char BUTTON_1 = 'b';
            char BUTTON_2 = '5';
            char BUTTON_3 = '6';

            {
                std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                if (shared_data_ui.control_input != 0) {
                    input = tolower(shared_data_ui.control_input);
                    shared_data_ui.control_input = 0;
                }

                scene = shared_data_ui.scene;
                scene_change_timestamp = this->shared_data_ui.scene_change_timestamp;
                variant = shared_data_ui.variant;
                variant_change_timestamp = this->shared_data_ui.variant_change_timestamp;

                trial = shared_data_ui.trial;
                total_trials = shared_data_ui.total_trials;

                distance_cm = shared_data_ui.distance_cm;
                max_distance_cm = shared_data_ui.max_distance_cm;
            }

            double distributions[] = {1., 1./1.5, 1.5}; // , 1./1.25, 1.25
            std::uniform_int_distribution<int> random_ratio_generator(0, DISTRIBUTION_SIZE - 1);
            std::uniform_real_distribution<double> random_distance_generator(1., max_distance_cm);

            auto t_now = std::chrono::high_resolution_clock::now();
            double scene_change_sec = std::chrono::duration<double>(t_now - scene_change_timestamp).count();

            if (input == 0) {
                return true;
            } else if (input == 'q') {
                spdlog::info("Ui.update: Exitting");
                return false;
            } else if (input == 'h') {
                spdlog::info("Ui.update: Help\n\t  BUTTON_1 = '{}'\n\t  BUTTON_2 = '{}'\n\t  BUTTON_3 = '{}'\n\tUniversal:\n\t  Q - Quit\n\t  H - Help\n\t  E - Trigger custom event\n\n\tInstruction/Push to start:\n\t  1 - Familiarization\n\t  2 - Tactile\n\t  3 - Movement\n\t  P - Next trial\n\t  O - Previous trial\n\t  R - update arm length (instruction screen only)\n\t  BUTTON_1 - Start trial\n\n\tTrial:\n\t  BUTTON_1 (Familiarization) - Back to instruction screen\n\t  BUTTON_1 (Other) - Go to judgement screen\n\n\tJudgement:\n\t  BUTTON_1 - Save length and go to Push to start\n\t  BUTTON_2 - Decrease length by 1 cm\n\t  BUTTON_3 - Increase length by 1 cm", BUTTON_1, BUTTON_2, BUTTON_3);
                return false;
            } else if (input == 'e') {
                label_timestamp("Custom_Event_Triggered");
            } else {
                bool update_scene = false;
                SCENE_TYPE new_scene;

                bool update_variant = false;
                VARIANT_TYPE new_variant;

                bool update_ratio = false;
                bool random_ratio = true;
                double new_ratio = 1.;

                bool update_trial = false;
                int new_trial = 1;
                bool update_total_trials = false;
                int new_total_trials = 1;

                bool save_distance = false;
                bool update_max_distance = false;
                bool update_distance = false;
                bool random_distance = false;
                double new_distance = 0.;

                bool will_exit = false;

                switch(scene) {
                    case SCENE_INSTRUCTION:
                        if (input == 'r') {
                            update_max_distance = true;
                        }
                        if (!loaded_hand_distance) {
                            update_max_distance = true;
                        }
                    case SCENE_PUSH_TO_START:
                        if (input == '1') {
                            update_variant = true;
                            new_variant = VARIANT_FAMILIARIZATION;
                            update_trial = true;
                            update_total_trials = true;
                        } else if (input == '2') {
                            update_variant = true;
                            new_variant = VARIANT_TOUCH;
                            update_trial = true;
                            update_total_trials = true;
                            new_total_trials = DISTRIBUTION_SIZE * SETS_OF_VALID_TRIALS + TOTAL_TRAINING_TRIALS;
                        } else if (input == '3') {
                            update_variant = true;
                            new_variant = VARIANT_MOVEMENT;
                            update_trial = true;
                            update_total_trials = true;
                            new_total_trials = DISTRIBUTION_SIZE * SETS_OF_VALID_TRIALS + TOTAL_TRAINING_TRIALS;
                        } else if (input == 'p') {
                            update_trial = true;
                            new_trial = trial + 1;
                        } else if (input == 'o') {
                            update_trial = true;
                            new_trial = trial - 1;
                        } else if (input == BUTTON_1) {
                            update_scene = true;
                            new_scene = SCENE_321;
                            update_ratio = true;

                            if (variant == VARIANT_FAMILIARIZATION || trial <= TOTAL_TRAINING_TRIALS) {
                                random_ratio = false; // uses default 1:1 ratio
                            }
                        }
                        break;
                    case SCENE_321:
                        if (input == BUTTON_1 && scene_change_sec > 3.5) {
                            if (variant == VARIANT_FAMILIARIZATION) {
                                update_scene = true;
                                new_scene = SCENE_INSTRUCTION;
                            } else {
                                update_scene = true;
                                new_scene = SCENE_JUDGEMENT;
                                update_distance = true;
                                random_distance = true;
                            }
                        }
                        break;
                    case SCENE_JUDGEMENT:
                        switch(variant) {
                            case VARIANT_TOUCH:
                            case VARIANT_MOVEMENT:
                                if (input == BUTTON_1 && scene_change_sec > 1.) {
                                    update_trial = true;
                                    new_trial = trial + 1;
                                    update_scene = true;
                                    if (new_trial <= total_trials) {
                                        new_scene = SCENE_PUSH_TO_START;
                                    } else {
                                        new_scene = SCENE_INSTRUCTION;
                                        will_exit = true;
                                    }
                                    save_distance = true;
                                } else if (input == BUTTON_2) {
                                    update_distance = true;
                                    new_distance = distance_cm - 1.;
                                } else if (input == BUTTON_3) {
                                    update_distance = true;
                                    new_distance = distance_cm + 1.;
                                }
                                break;
                            default:
                                spdlog::error("Ui.update: Unknown combination scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
                                break;
                        }
                        break;
                    default:
                        spdlog::error("Ui.update: Unknown scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
                        break;
                }

                if (update_scene) {
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.scene = new_scene;
                        shared_data_ui.scene_change_timestamp = t_now;
                    }

                    std::string msg = "";
                    switch (new_scene) {
                        case SCENE_INSTRUCTION:
                            msg = "EXPERIMENT_START";
                            break;
                        case SCENE_PUSH_TO_START:
                            break;
                        case SCENE_321:
                            msg = "TRIAL_WILL_START";
                            break;
                        case SCENE_JUDGEMENT:
                            msg = "TRIAL_DONE";
                            break;
                    }
                    if (msg != "") label_timestamp(msg);
                    //spdlog::info("Ui.update: Updated scene to " + std::to_string(new_scene));
                }

                if (update_variant) {
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.variant = new_variant;
                        shared_data_ui.variant_change_timestamp = t_now;
                    }

                    std::string msg = "";
                    switch (new_variant) {
                        case VARIANT_FAMILIARIZATION:
                            msg = "VARIANT_FAMILIARIZATION";
                            break;
                        case VARIANT_TOUCH:
                            msg = "VARIANT_TOUCH";
                            break;
                        case VARIANT_MOVEMENT:
                            msg = "VARIANT_MOVEMENT";
                            break;
                    }
                    if (msg != "") label_timestamp(msg);
                    //spdlog::info("Ui.update: Updated variant to " + std::to_string(new_variant));
                }

                if (update_trial) {
                    new_trial = std::max(1, std::min(new_trial, total_trials));
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.trial = new_trial;
                        trial = new_trial;
                    }
                    label_timestamp("TRIAL_" + std::to_string(new_trial));
                    //spdlog::info("Ui.update: Updated trial to " + std::to_string(new_trial));
                }

                if (update_ratio) {
                    if (random_ratio) {
                        new_ratio = random_trails[trial - 1 - TOTAL_TRAINING_TRIALS];
                    }
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.ratio = new_ratio;
                    }
                    label_timestamp("RATIO_" + to_string_with_precision(new_ratio, 2));
                    //spdlog::info("Ui.update: Updated ratio to " + std::to_string(new_ratio));
                }

                if (update_total_trials) {
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.total_trials = new_total_trials;
                    }
                    random_trails = std::vector<double>(new_total_trials - TOTAL_TRAINING_TRIALS);
                    for (int i = 0; i < new_total_trials - TOTAL_TRAINING_TRIALS; i++) {
                        random_trails[i] = distributions[i % DISTRIBUTION_SIZE];
                    }
                    std::shuffle(random_trails.begin(), random_trails.end(), random_engine);
                    label_timestamp("TOTAL_TRIALS_" + std::to_string(new_total_trials));

                    //spdlog::info("Ui.update: Updated total trials to " + std::to_string(new_total_trials));
                }

                if (update_distance) {
                    if (random_distance) {
                        new_distance = std::round(random_distance_generator(random_engine));
                    }
                    distance_cm = std::max(1., std::min(new_distance, SCREEN_HEIGHT_CM));
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        shared_data_ui.distance_cm = distance_cm;
                    }
                    //label_timestamp("Distance_Updated_" + std::to_string(distance_cm));
                    //spdlog::info("Ui.update: Updated distance to " + std::to_string(distance_cm));
                }

                if (update_max_distance) {
                    double new_max_distance = 0.;
                    {
                        std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                        if (shared_data_camera.hand_left_pos != Vector3d::Zero() && shared_data_camera.elbow_pos != Vector3d::Zero()) {
                        new_max_distance = (shared_data_camera.hand_left_pos - shared_data_camera.elbow_pos).norm() * 100; // cm;
                        }
                    }

                    if (new_max_distance > 0.) {
                        loaded_hand_distance = true;
                        max_distance_cm = std::round(std::max(1., new_max_distance));
                        {
                            std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                            shared_data_ui.max_distance_cm = max_distance_cm;
                        }
                        label_timestamp("ARM_LENGTH_CM_" + to_string_with_precision(max_distance_cm,0));
                    } else {
                        spdlog::warn("Ui.update: Could not load max distance");
                    }
                    //spdlog::info("Ui.update: Updated max distance to " + std::to_string(max_distance_cm));
                }

                if (save_distance) {
                    label_timestamp("JUDGEMENT_LENGTH_CM_" + to_string_with_precision(distance_cm,0));
                    //spdlog::info("Ui.update: Saved distance to " + std::to_string(distance_cm));
                }

                if (will_exit) {
                    spdlog::info("Ui.update: Exitting");
                    return false;
                }
            }            

            return true;
        }
};

#endif // UI_H
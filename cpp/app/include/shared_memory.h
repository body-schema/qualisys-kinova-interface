#ifndef SHARED_MEMORY_H
#define SHARED_MEMORY_H

#pragma once


#include <Eigen/Dense>
#include <chrono>

enum SCENE_TYPE {
    SCENE_INSTRUCTION,
    SCENE_PUSH_TO_START,
    SCENE_321,
    SCENE_JUDGEMENT
};

enum VARIANT_TYPE {
    VARIANT_FAMILIARIZATION,
    VARIANT_TOUCH,
    VARIANT_MOVEMENT
};

enum TRAJECTORY_TYPE {
    TRAJECTORY_RIGHT,
    TRAJECTORY_HOME_RIGHT,
    TRAJECTORY_HOME_RANDOM
};

struct SharedDataCamera {
    Eigen::Vector3d hand_pos = Eigen::Vector3d::Zero();
    Eigen::Vector3d hand_vel = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point hand_measurement_timestamp;
    Eigen::Vector3d robot_pos = Eigen::Vector3d::Zero();
    Eigen::Vector3d robot_vel = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point robot_measurement_timestamp;
    Eigen::Vector3d hand_left_pos = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point hand_left_measurement_timestamp;
    Eigen::Vector3d elbow_pos = Eigen::Vector3d::Zero();
    std::chrono::high_resolution_clock::time_point elbow_measurement_timestamp;
};

struct SharedDataRobot {
    Eigen::Matrix<double,7,1> joint_angles = Eigen::Matrix<double,7,1>::Zero();
    Eigen::Matrix<double,7,1> angular_velocities = Eigen::Matrix<double,7,1>::Zero();
    std::chrono::high_resolution_clock::time_point joint_measurement_timestamp;
    Eigen::Matrix<double,7,1> goal_joint_angles = Eigen::Matrix<double,7,1>::Zero();
    std::chrono::high_resolution_clock::time_point goal_computed_timestamp;
    std::chrono::high_resolution_clock::time_point joint_control_timestamp;
};

struct SharedDataUi {
    int participant_id;
    bool event_trigger = false;
    std::string event_message;

    char control_input = 0;
    double ratio = 1.;
    
    SCENE_TYPE scene = SCENE_INSTRUCTION; // Default scene
    std::chrono::high_resolution_clock::time_point scene_change_timestamp;
    VARIANT_TYPE variant = VARIANT_FAMILIARIZATION; // Default variant
    std::chrono::high_resolution_clock::time_point variant_change_timestamp;

    int trial = 1;
    int total_trials = 1;

    double distance_cm = 1.;
    double max_distance_cm = 35.;
};

#endif // SHARED_MEMORY_H
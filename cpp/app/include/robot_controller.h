#ifndef ROBOT_CONTROLLER_H
#define ROBOT_CONTROLLER_H

#pragma once


#include <Eigen/Dense>
#include <thread>
#include <deque>
#include <utility>
#include <random>
#include "spdlog/spdlog.h"

#include "json.h"
#include "kortex_controller.h"
#include "ik_computer.h"

using json = nlohmann::json;
using TimePoint = std::chrono::high_resolution_clock::time_point;
using Point = std::pair<Eigen::Vector3d, TimePoint>;

// Assuming 0.1mm might be detectable by the FK model
#define DETECTABLE_DISTANCE_M 0.0001

struct Transformation {
    Eigen::Matrix3d rotation = Eigen::Matrix3d::Zero();
    Eigen::Vector3d translation = Eigen::Vector3d::Zero();
};

class MovingWindow {
    private:
        size_t window_size_;
        int decay_ms_;
        std::deque<Point> points_;

        void clear_points() {
            TimePoint now = std::chrono::high_resolution_clock::now();
            while (!points_.empty() && (now - points_.front().second) > std::chrono::milliseconds(decay_ms_)) {
                points_.pop_front();
            }
        }

    public:
        MovingWindow(size_t window_size, int decay_ms) : window_size_(window_size), decay_ms_(decay_ms) {}

        void push_back(const Eigen::Vector3d& point, const TimePoint& time) {
            if (points_.empty() || points_.back().second != time) {
                points_.emplace_back(point, time);
                while (!points_.empty() && points_.size() > window_size_) {
                    points_.pop_front();
                }
            }
        }

        const std::deque<Point>& points() {
            clear_points();
            return points_;
        }

        const Point& front() {
            clear_points();
            return points_.front();
        }

        const Point& back() {
            clear_points();
            return points_.back();
        }
};

class RobotController {
    private:
        mINI::INIStructure& config;
        SharedDataCamera& shared_data_camera;
        std::mutex& shared_data_camera_mutex;
        SharedDataUi& shared_data_ui;
        std::mutex& shared_data_ui_mutex;
        KortexController* kc = nullptr;
        IKComputer* ikc = nullptr;
        Transformation TRANSFORMATION;
        double X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN, Z_MAX;
        double MAX_VEL_NORM, MAX_VEL_COMPENSATION_MULTIPLIER, MAX_VEL_DECREASE_MULTIPLIER, MAX_DIST_RECOVERY_MULTIPLIER;
        Eigen::AlignedBox3d BOUNDARY_BOX;
        Eigen::Vector3d MIN_VECT, MAX_VECT;
        Eigen::Vector3d ROBOT_POS_INIT_R;
        int MOVING_WINDOW_SIZE, MOVING_WINDOW_DEADLINE_MS;
        double MOVING_WINDOW_ACTIVE_FROM_SEC;
        std::mt19937 random_engine;
        std::uniform_real_distribution<double> random_hand_offset_x, random_hand_offset_y, random_hand_offset_z;

    public:
        const Eigen::Vector3d HAND_AXES = Eigen::Vector3d(1., 0., 0.);
        const Eigen::Vector3d HAND_AXES_INVERSE = -1*(HAND_AXES - Eigen::Vector3d::Ones());
    
        RobotController(mINI::INIStructure& config,
                SharedDataCamera& shared_data_camera,
                std::mutex& shared_data_camera_mutex,
                SharedDataUi& shared_data_ui,
                std::mutex& shared_data_ui_mutex)
        : config(config),
        shared_data_camera(shared_data_camera),
        shared_data_camera_mutex(shared_data_camera_mutex),
        shared_data_ui(shared_data_ui),
        shared_data_ui_mutex(shared_data_ui_mutex) {
            try {
                ROBOT_POS_INIT_R = Eigen::Vector3d(
                    std::stod(config["RobotController"]["x_init"]),
                    std::stod(config["RobotController"]["y_init"]),
                    std::stod(config["RobotController"]["z_init"])
                );
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse x_init, y_init, z_init: {}", e.what());
                throw std::invalid_argument("RobotController: x,y,z initial position must be set");
            }
            try {
                MAX_VEL_NORM = std::stod(config["RobotController"]["norm_cartesian_vel_max"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse norm_cartesian_vel_max: {}", e.what());
                throw std::invalid_argument("RobotController: Cartesian maximal velocity norm must be set");
            }
            try {
                MAX_VEL_COMPENSATION_MULTIPLIER = std::stod(config["RobotController"]["max_vel_compensation_multiplier"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse max_vel_compensation_multiplier: {}", e.what());
                throw std::invalid_argument("RobotController: Norm compensation multiplier must be set");
            }
            try {
                MAX_VEL_DECREASE_MULTIPLIER = std::stod(config["RobotController"]["max_vel_decrease_multiplier"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse max_vel_decrease_multiplier: {}", e.what());
                throw std::invalid_argument("RobotController: Max vel decreasing multiplier must be set");
            }
            try {
                MAX_DIST_RECOVERY_MULTIPLIER = std::stod(config["RobotController"]["max_dist_recovery_multiplier"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse max_dist_recovery_multiplier: {}", e.what());
                throw std::invalid_argument("RobotController: Max distance recovery multiplier must be set");
            }
            try {
                MOVING_WINDOW_SIZE = std::stoi(config["RobotController"]["moving_window_size"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse moving_window_size: {}", e.what());
                throw std::invalid_argument("RobotController: Moving window size must be set");
            }
            try {
                MOVING_WINDOW_DEADLINE_MS = std::stoi(config["RobotController"]["moving_window_deadline_ms"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse moving_window_deadline_ms: {}", e.what());
                throw std::invalid_argument("RobotController: Moving window deadline for deletion must be set");
            }
            try {
                MOVING_WINDOW_ACTIVE_FROM_SEC = 0.01*std::stod(config["RobotController"]["moving_window_active_from_ms"]);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse moving_window_active_from_ms: {}", e.what());
                throw std::invalid_argument("RobotController: Moving window activation zone must be set");
            }
            try {
                double hand_random_init_x_min = std::stod(config["RobotController"]["x_random_init_min"]);
                double hand_random_init_x_max = std::stod(config["RobotController"]["x_random_init_max"]);
                double hand_random_init_y_min = std::stod(config["RobotController"]["y_random_init_min"]);
                double hand_random_init_y_max = std::stod(config["RobotController"]["y_random_init_max"]);
                double hand_random_init_z_min = std::stod(config["RobotController"]["z_random_init_min"]);
                double hand_random_init_z_max = std::stod(config["RobotController"]["z_random_init_max"]);

                std::random_device rd;  // Will be used to obtain a seed for the random number engine
                random_engine = std::mt19937(rd()); // Standard mersenne_twister_engine seeded with rd()
                random_hand_offset_x = std::uniform_real_distribution<double>(hand_random_init_x_min, hand_random_init_x_max);
                random_hand_offset_y = std::uniform_real_distribution<double>(hand_random_init_y_min, hand_random_init_y_max);
                random_hand_offset_z = std::uniform_real_distribution<double>(hand_random_init_z_min, hand_random_init_z_max);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse x_random_init_min, x_random_init_max: {}", e.what());
                throw std::invalid_argument("RobotController: Initial random offset position must be set");
            }
            try {
                X_MIN = std::stod(config["RobotController"]["x_min"]);
                X_MAX = std::stod(config["RobotController"]["x_max"]);
                Y_MIN = std::stod(config["RobotController"]["y_min"]);
                Y_MAX = std::stod(config["RobotController"]["y_max"]);
                Z_MIN = std::stod(config["RobotController"]["z_min"]);
                Z_MAX = std::stod(config["RobotController"]["z_max"]);
                MIN_VECT = Eigen::Vector3d(X_MIN, Y_MIN, Z_MIN);
                MAX_VECT = Eigen::Vector3d(X_MAX, Y_MAX, Z_MAX);
                BOUNDARY_BOX = Eigen::AlignedBox3d(MIN_VECT, MAX_VECT);
            }  catch (std::exception& e) {
                spdlog::error("RobotController: Unable to parse x_min, x_max, y_min, y_max, z_min, z_max: {}", e.what());
                throw std::invalid_argument("RobotController: x,y,z limits must be set");
            }
            spdlog::info("RobotController: XYZ settings: x<{}; {}>, y<{}; {}>, z<{}; {}>", X_MIN, X_MAX, Y_MIN, Y_MAX, Z_MIN, Z_MAX);
            if (config["RobotController"]["transformation"].length() > 0) {
                try {
                    json data = json::parse(config["RobotController"]["transformation"]);
                    Transformation t;
                    for (int i=0; i<3; i++) {
                        for (int j=0; j<3; j++) {
                            t.rotation(i,j) = data[0][i][j];
                        }
                        t.translation(i) = data[1][i][0];
                    }
                    spdlog::info("RobotController: Using calibration from settings");
                    TRANSFORMATION = t;
                } catch (std::exception& e) {
                    spdlog::error("RobotController: Unable to parse transformation json, {}", e.what());
                }
            }
        }

        Eigen::Vector3d transform_rob2cam(Eigen::Vector3d position) {
            return (TRANSFORMATION.rotation * position) + TRANSFORMATION.translation;
        }

        Eigen::Vector3d transform_cam2rob(Eigen::Vector3d position) {
            return (TRANSFORMATION.rotation.transpose() * (position - TRANSFORMATION.translation));
        }

        Eigen::Vector3d generate_linear_trajectory(
            TimePoint currentTimestamp,
            TimePoint beginningTimestamp,
            double duration_seconds,
            Eigen::Vector3d startPosition,
            Eigen::Vector3d goalPosition
        ) {
            if (currentTimestamp < beginningTimestamp || duration_seconds <= 0) {
                return startPosition;
            }

            double elapsedTime = std::chrono::duration<double>(currentTimestamp - beginningTimestamp).count();
            double progress = elapsedTime / duration_seconds;

            if (progress > 1) {
                progress = 1;
            }

            Eigen::Vector3d currentPosition = startPosition + progress * (goalPosition - startPosition);
            return currentPosition;
        }


        bool calibration() {
            if (kc == nullptr) {
                spdlog::error("RobotController.calibration: Unable to run calibration, kortex not initialized.");
                return false;
            }

            spdlog::info("RobotController.calibration: Waiting for robot position from camera");
            Eigen::Vector3d robot_position_c = Eigen::Vector3d::Zero();
            while (robot_position_c.isZero()) {
                {
                    std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                    robot_position_c = shared_data_camera.robot_pos;
                }
                if (!robot_position_c.isZero()) break;
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
            }

            spdlog::info("RobotController.calibration: Running calibration");

            int calibration_size = std::stoi(config["RobotController"]["calibration_size"]);
            calibration_size = calibration_size - calibration_size % 10;
            Eigen::MatrixXd camera_set(3, calibration_size);
            Eigen::MatrixXd robot_set(3, calibration_size);

            Eigen::Vector3d robot_position_r;
            auto mv_robot = [&](float n, int i, Eigen::Vector3d velocity) {
                kc->goContinuous(velocity * n);
                std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<int>(std::round(500. * n))));
                kc->goContinuous();
                std::this_thread::sleep_for(std::chrono::milliseconds(300));

                {
                    std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                    robot_position_c = shared_data_camera.robot_pos;
                }
                robot_position_r = kc->feedback();

                camera_set(0,i) = robot_position_c.x();
                camera_set(1,i) = robot_position_c.y();
                camera_set(2,i) = robot_position_c.z();

                robot_set(0,i) = robot_position_r.x();
                robot_set(1,i) = robot_position_r.y();
                robot_set(2,i) = robot_position_r.z();
            };

            // Init safe position
            kc->goHome();

            kc->goContinuous(Eigen::Vector3d(0.2, 0.0, 0.));
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
            kc->goContinuous(Eigen::Vector3d(0., 0., 0.));

            float n=1.f;
            for (int i=0; i<calibration_size;) {
                mv_robot(n, i++, Eigen::Vector3d(-.15, -.1, -.1));
                mv_robot(n, i++, Eigen::Vector3d(-.1, .1, 0.));
                mv_robot(n, i++, Eigen::Vector3d(.15, -.15, 0.));
                mv_robot(n, i++, Eigen::Vector3d(0., .15, .1));
                mv_robot(n, i++, Eigen::Vector3d(.1, .1, 0.));
                mv_robot(n, i++, Eigen::Vector3d(.12, -.12, .15));
                mv_robot(n, i++, Eigen::Vector3d(-.12, .12, -.15));
                mv_robot(n, i++, Eigen::Vector3d(0., -.2, 0.));
                mv_robot(n, i++, Eigen::Vector3d(0., .2, 0.2));
                mv_robot(n, i++, Eigen::Vector3d(0., -.1, -0.2));
                n *= .8;
            }
            kc->goHome();
            
            // Find transformation
            Eigen::Matrix t = Eigen::umeyama(robot_set, camera_set, false);
            for (int i=0; i<3; i++) {
                for (int j=0; j<3; j++) {
                    TRANSFORMATION.rotation(i,j) = t(i,j);
                }
                TRANSFORMATION.translation(i) = t(i,3);
            }

            std::string json_transformation = "[[";
            for (int i=0; i<3; i++) {
                json_transformation += "[";
                for (int j=0; j<3; j++) {
                    json_transformation += std::to_string(TRANSFORMATION.rotation(i, j));
                    if (j < 2) json_transformation += ", ";
                }
                if (i<2) json_transformation += "],";
                else json_transformation += "]],[["+std::to_string(TRANSFORMATION.translation.x())+"], ["+std::to_string(TRANSFORMATION.translation.y())+"], ["+std::to_string(TRANSFORMATION.translation.z())+"]]]";
            }

            // Calculate RMSE:
            Eigen::MatrixXd robot_set_calc = TRANSFORMATION.rotation * robot_set + TRANSFORMATION.translation;
            Eigen::MatrixXd error = (robot_set_calc - camera_set);
            float rmse = sqrt((error * error).sum()/calibration_size);
            spdlog::info("RobotController.calibration: RMSE {}", rmse);
            spdlog::info("RobotController.calibration: Transformation done {}", json_transformation);
            return true;
        }

        void check_robot_model_FK(std::stop_token should_run, KortexController* kc) {
            // Way of checking the robot model vs reality
            ikc = new IKComputer(kc->feedbackRotation());
            Eigen::Vector3d positionDiffAvg = Eigen::Vector3d(0,0,0);
            double counter = 0;
            while (!should_run.stop_requested()) {
                Eigen::Matrix4d robot_ik = kc->full_feedback();
                Eigen::Matrix4d model_ik = ikc->fk_full(kc->get_joint_angles());
                Eigen::Matrix4d diff = (robot_ik - model_ik);
                Eigen::Vector3d positionDiff = Eigen::Vector3d(diff(0,3), diff(1,3), diff(2,3));
                positionDiffAvg += positionDiff;
                counter += 1;
                std::cout << "\n\nRobot-Model FK norm:" << positionDiffAvg.norm()/counter << std::endl;
            }
        }

        Eigen::Vector3d compute_trajectory_goal(
            TRAJECTORY_TYPE &trajectory,
            TRAJECTORY_TYPE &prev_trajectory,
            Eigen::Vector3d &robot_pos,
            Eigen::Vector3d &trajectory_start_pos,
            Eigen::Vector3d &trajectory_random_pos,
            TimePoint &trajectory_start_time,
            bool& trajectory_done
        ) {
            // All the inputs are in robot frame
            Eigen::Vector3d robot_target_pos = robot_pos;
            Eigen::Vector3d trajectory_goal_pos = robot_pos;
            double trajectory_duration_sec = 0.;
            TimePoint t_now = std::chrono::high_resolution_clock::now();

            if (prev_trajectory != trajectory) {
                trajectory_start_pos = robot_pos; // Load current position and go to goal from there
                trajectory_start_time = t_now;
                trajectory_done = false;
                if (trajectory == TRAJECTORY_HOME_RANDOM) {
                    trajectory_random_pos = Eigen::Vector3d(random_hand_offset_x(random_engine), random_hand_offset_y(random_engine), random_hand_offset_z(random_engine));
                }
            }

            switch(trajectory) {
                case TRAJECTORY_RIGHT:
                    //std::cout << "TRAJECTORY_RIGHT" << std::endl;
                    trajectory_goal_pos = trajectory_start_pos + Eigen::Vector3d(0., 0.05, 0.);
                    trajectory_duration_sec = .5;
                    break;
                case TRAJECTORY_HOME_RIGHT:
                    //std::cout << "TRAJECTORY_HOME_RIGHT" << std::endl;
                    trajectory_goal_pos = ROBOT_POS_INIT_R + Eigen::Vector3d(0., 0.05, 0.);
                    trajectory_duration_sec = 2.;
                    break;
                case TRAJECTORY_HOME_RANDOM:
                    //std::cout << "TRAJECTORY_HOME_RANDOM" << std::endl;
                    trajectory_goal_pos = ROBOT_POS_INIT_R + trajectory_random_pos;
                    trajectory_duration_sec = 1.;
                    break;
            }

            robot_target_pos = generate_linear_trajectory(
                t_now,
                trajectory_start_time,
                trajectory_duration_sec,
                trajectory_start_pos,
                trajectory_goal_pos
            );
            prev_trajectory = trajectory;
            if (robot_target_pos == trajectory_goal_pos) {
                trajectory_done = true;
            }

            Eigen::Vector3d robot_target_pos_limited = robot_target_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
            return robot_target_pos_limited;
        }

        Eigen::Vector3d compute_goal_position(
            Eigen::Vector3d& hand_pos,
            Eigen::Vector3d& hand_vel,
            TimePoint& hand_measurement_timestamp,
            Eigen::Vector3d& hand_offset_pos,
            Eigen::Vector3d& robot_offset_pos,
            Eigen::Vector3d& trajectory_pos,
            double& ratio
        ) {
            // All the inputs are in robot frame
            Eigen::Vector3d robot_target_pos;

            if (trajectory_pos == Eigen::Vector3d::Zero()) {
                Eigen::Vector3d hand_traget_pos = (hand_pos - hand_offset_pos)*ratio;
                Eigen::Vector3d robot_pos = ROBOT_POS_INIT_R;
                robot_target_pos = robot_pos.cwiseProduct(HAND_AXES_INVERSE) + (robot_offset_pos + hand_traget_pos).cwiseProduct(HAND_AXES);
            } else {
                robot_target_pos = trajectory_pos;
            }

            Eigen::Vector3d robot_target_pos_limited = robot_target_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
            return robot_target_pos_limited;
        }

        bool verify_position(Eigen::Vector3d& goal_pos) {
            // All the inputs are in robot frame
            return BOUNDARY_BOX.contains(goal_pos);
        }

        Eigen::Vector3d compute_intergoal_position(
            MovingWindow& mw_robot_pos,
            Eigen::Vector3d& goal_pos,
            TimePoint& joint_control_timestamp,
            bool& previously_limited_intergoal
        ) {
            // All the inputs are in robot frame
            Eigen::Vector3d intergoal_pos = goal_pos;
            const Point& point = mw_robot_pos.points().back();
            Eigen::Vector3d robot_pos = point.first;

            TimePoint now = std::chrono::high_resolution_clock::now();
            double dt = std::chrono::duration<double>(now - joint_control_timestamp).count();

            // max distance from the last frame - first limiting factor
            double max_distance = MAX_VEL_NORM * MAX_VEL_COMPENSATION_MULTIPLIER * dt;

            // calculate average velocity
            const std::deque<Point>& points = mw_robot_pos.points();
            double sum_distance = 0.;
            double sum_time = 0.;
            for (const Point& historical_point : points) {
                double historical_dt = std::chrono::duration<double>(now - historical_point.second).count();
                if (historical_dt > MOVING_WINDOW_ACTIVE_FROM_SEC) { // Take samples atleast 20ms old, otherwise, there is too much noise
                    sum_distance += (robot_pos - historical_point.first).norm();
                    sum_time += historical_dt;
                }
            }
            double average_velocity = 0.;
            if (sum_time != 0.) {
                average_velocity = sum_distance / sum_time;
            }

            double distance = (robot_pos - intergoal_pos).norm();

            // If the change is lower than the max distance travelable in up to 1ms
            if (((previously_limited_intergoal && distance < max_distance * MAX_DIST_RECOVERY_MULTIPLIER) || (!previously_limited_intergoal && distance < max_distance)) && average_velocity < MAX_VEL_NORM) {
                previously_limited_intergoal = false;
                return goal_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
            } else {
                // Limit the distance to the max distance travelable in 1ms
                previously_limited_intergoal = true;
                double distance_multi = max_distance * MAX_VEL_DECREASE_MULTIPLIER;
                intergoal_pos = ((robot_pos + (goal_pos - robot_pos).normalized() * distance_multi).cwiseProduct(HAND_AXES) + ROBOT_POS_INIT_R.cwiseProduct(HAND_AXES_INVERSE)).cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);

                return intergoal_pos.cwiseMax(MIN_VECT).cwiseMin(MAX_VECT);
            }
        }

        Eigen::Vector3d get_hand_offset_pos() {
            std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
            return transform_cam2rob(shared_data_camera.hand_pos);
        }

        void get_shared_data(
            SharedDataRobot& shared_data_robot,
            std::mutex& shared_data_robot_mutex,
            double& ratio,
            SCENE_TYPE& scene,
            TimePoint& scene_change_timestamp,
            double& max_distance_cm,
            Eigen::Vector3d& hand_pos_r,
            Eigen::Vector3d& hand_vel, // should be in _r also
            TimePoint& hand_measurement_timestamp,
            Matrix<double,7,1>& joint_angles,
            Matrix<double,7,1>& angular_velocities,
            TimePoint& joint_measurement_timestamp,
            TimePoint& joint_control_timestamp
        ) {
            // Get shared data
            {
                std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                ratio = shared_data_ui.ratio;
                scene = shared_data_ui.scene;
                scene_change_timestamp = shared_data_ui.scene_change_timestamp;
                max_distance_cm = shared_data_ui.max_distance_cm;
            }
            {
                std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                hand_pos_r = transform_cam2rob(shared_data_camera.hand_pos);
                hand_vel = shared_data_camera.hand_vel;
                hand_measurement_timestamp = shared_data_camera.hand_measurement_timestamp;
            }
            {
                std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                joint_angles = shared_data_robot.joint_angles;
                angular_velocities = shared_data_robot.angular_velocities;
                joint_measurement_timestamp = shared_data_robot.joint_measurement_timestamp;
                joint_control_timestamp = shared_data_robot.joint_control_timestamp;
            }
        }

        void operator()(std::stop_token should_run) {
            // initialize shared variables
            SharedDataRobot shared_data_robot;
            std::mutex shared_data_robot_mutex;

            // run loop
            while (!should_run.stop_requested()) {
                try {
                    // Init KortexController
                    kc = new KortexController(config, shared_data_robot, shared_data_robot_mutex);

                    // Init calibration if there is no transformation
                    if (TRANSFORMATION.rotation.isZero() && TRANSFORMATION.translation.isZero()) {
                        calibration();
                    }
                    // Init starting position
                    kc->goPosition(ROBOT_POS_INIT_R, Eigen::Vector3d(180., 0., 90.));

                    while (!(kc->feedback() - ROBOT_POS_INIT_R).isZero(DETECTABLE_DISTANCE_M) && !should_run.stop_requested()) {
                        std::this_thread::sleep_for(std::chrono::seconds(1));
                    }
                    std::this_thread::sleep_for(std::chrono::seconds(1));

                    // Init inverse kinematics computer with initial feedback rotation
                    ikc = new IKComputer(kc->feedbackRotation());

                    // Run real-time loop thread
                    std::jthread kortexThread(*kc);

                    // Setup variables
                    Eigen::Vector3d robot_offset_pos_r = ROBOT_POS_INIT_R;

                    SCENE_TYPE scene;
                    TimePoint scene_change_timestamp;
                    double max_distance_cm;
                    double ratio;

                    Eigen::Vector3d hand_pos_r, hand_vel, hand_offset_pos_r;
                    TimePoint hand_measurement_timestamp;

                    Matrix<double,7,1> joint_angles;
                    Matrix<double,7,1> angular_velocities;
                    Matrix<double,7,1> goal_joint_angles;
                    TimePoint joint_measurement_timestamp;
                    TimePoint goal_computed_timestamp;
                    TimePoint joint_control_timestamp;

                    MovingWindow mw_robot_pos_r(MOVING_WINDOW_SIZE, MOVING_WINDOW_DEADLINE_MS);
                    mw_robot_pos_r.push_back(ROBOT_POS_INIT_R, std::chrono::high_resolution_clock::now());

                    bool previously_limited_intergoal = false;
                    
                    TimePoint trajectory_start_time = std::chrono::high_resolution_clock::now();
                    Eigen::Vector3d trajectory_start_pos_r = ROBOT_POS_INIT_R;
                    Eigen::Vector3d trajectory_pos_r;
                    Eigen::Vector3d trajectory_random_pos_r = Eigen::Vector3d::Zero();

                    bool hand_init_offset = true;

                    TRAJECTORY_TYPE trajectory = TRAJECTORY_HOME_RANDOM;
                    TRAJECTORY_TYPE prev_trajectory = TRAJECTORY_HOME_RANDOM;
                    bool trajectory_done = false;

                    bool first_init = true;

                    while (!should_run.stop_requested()) {
                        get_shared_data(
                            shared_data_robot,
                            shared_data_robot_mutex,
                            ratio,
                            scene,
                            scene_change_timestamp,
                            max_distance_cm,
                            hand_pos_r,
                            hand_vel,
                            hand_measurement_timestamp,
                            joint_angles,
                            angular_velocities,
                            joint_measurement_timestamp,
                            joint_control_timestamp
                        );

                        // If the robot is not initialized, wait for the joint angles
                        if (joint_angles.isZero()) {
                            std::this_thread::sleep_for(std::chrono::microseconds(50));
                            continue;
                        }

                        Eigen::Vector3d robot_pos_r = ikc->fk(joint_angles);
                        mw_robot_pos_r.push_back(robot_pos_r, joint_measurement_timestamp);

                        TimePoint t_now = std::chrono::high_resolution_clock::now();
                        double scene_change_sec = std::chrono::duration<double>(t_now - scene_change_timestamp).count();

                        bool use_trajectory = true;
                        if (scene == SCENE_321) {
                            first_init = false;
                            if (scene_change_sec > 2.) {
                                if (trajectory_done && trajectory == TRAJECTORY_HOME_RIGHT) {
                                    trajectory = TRAJECTORY_HOME_RANDOM;
                                    hand_init_offset = true;
                                } else if (trajectory_done && trajectory == TRAJECTORY_HOME_RANDOM) {
                                    if (hand_init_offset) {
                                        hand_init_offset = false;
                                        hand_offset_pos_r = hand_pos_r;
                                        robot_offset_pos_r = robot_pos_r;
                                        {
                                            std::string event_message = "TRACKING_STARTED";
                                            std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                                            if (!shared_data_ui.event_trigger) {
                                                shared_data_ui.event_message = event_message;
                                                shared_data_ui.event_trigger = true;
                                                // spdlog::info("Ui.label_timestamp: Labeled recording timestamp with '{}'", event_message);
                                            } else {
                                                shared_data_ui.event_message += ";" + event_message;
                                                shared_data_ui.event_trigger = true;
                                                // spdlog::info("Ui.label_timestamp: Labeled recording timestamp with '{}'", event_message);
                                            }
                                        }
                                    }
                                    use_trajectory = false; // start hand_tracking
                                }
                            }
                        } else {
                            if (trajectory_done && trajectory == TRAJECTORY_HOME_RANDOM && !first_init) {
                                trajectory = TRAJECTORY_RIGHT;
                            } else if (trajectory_done && trajectory == TRAJECTORY_RIGHT) {
                                trajectory = TRAJECTORY_HOME_RIGHT;
                            }
                        }

                        if (use_trajectory) {
                            trajectory_pos_r = compute_trajectory_goal(
                                trajectory,
                                prev_trajectory,
                                robot_pos_r,
                                trajectory_start_pos_r,
                                trajectory_random_pos_r,
                                trajectory_start_time,
                                trajectory_done
                            );
                        } else {
                            trajectory_pos_r = Eigen::Vector3d::Zero();
                        }

                        Eigen::Vector3d goal_pos_r = compute_goal_position(
                            hand_pos_r,
                            hand_vel,
                            hand_measurement_timestamp,
                            hand_offset_pos_r,
                            robot_offset_pos_r,
                            trajectory_pos_r,
                            ratio
                        );

                        Eigen::Vector3d intergoal_pos_r = compute_intergoal_position(mw_robot_pos_r, goal_pos_r, joint_control_timestamp, previously_limited_intergoal);
                        // std::cout << "goal_pos_r: " << goal_pos_r.transpose() << " trajectory:" << trajectory_pos_r.transpose()  << " intergoal:" << intergoal_pos_r.transpose() << std::endl;
                        
                        if (verify_position(intergoal_pos_r)) {
                            if (ikc->ik(joint_angles, intergoal_pos_r))  {
                                std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                                goal_joint_angles = ikc->Q_star;
                                shared_data_robot.goal_joint_angles = goal_joint_angles;
                                shared_data_robot.goal_computed_timestamp = std::chrono::high_resolution_clock::now();
                            }
                        }
                    std::this_thread::sleep_for(std::chrono::microseconds(50));
                    }
                    kortexThread.request_stop();
                    kortexThread.join();
                } catch (std::exception& e) {
                    spdlog::error("RobotController(): {}", e.what());
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(50));
            }
        }
};

#endif // ROBOT_CONTROLLER_H
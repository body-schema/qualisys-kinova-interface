#ifndef KORTEX_CONTROLLER_H
#define KORTEX_CONTROLLER_H

#pragma once


#include <BaseClientRpc.h>
#include <BaseCyclicClientRpc.h>
#include <ActuatorConfigClientRpc.h>
#include <SessionClientRpc.h>
#include <SessionManager.h>

#include <RouterClient.h>
#include <TransportClientTcp.h>
#include <TransportClientUdp.h>
#include <KDetailedException.h>

#include <google/protobuf/util/json_util.h>

#include <Eigen/Dense>
#include "pid.h"

#if defined(_MSC_VER)
#include <Windows.h>
#else
#include <unistd.h>
#endif
#include <time.h>

#include "spdlog/spdlog.h"

namespace k_api = Kinova::Api;


std::function<void(k_api::Base::ActionNotification)> 
check_for_end_or_abort(bool& finished)
{
    return [&finished](k_api::Base::ActionNotification notification)
    {
        //std::cout << "EVENT : " << k_api::Base::ActionEvent_Name(notification.action_event()) << std::endl;

        // The action is finished when we receive a END or ABORT event
        switch(notification.action_event())
        {
        case k_api::Base::ActionEvent::ACTION_ABORT:
        case k_api::Base::ActionEvent::ACTION_END:
            finished = true;
            break;
        default:
            break;
        }
    };
}

double angle_difference(double angle1, double angle2) {
    double diff = fmod(fmod(angle2, 360.) - fmod(angle1, 360.) + 180., 360.) - 180.;
    return diff < -180. ? diff + 360. : diff;
}

int64_t GetTickUs() {
#if defined(_MSC_VER)
    LARGE_INTEGER start, frequency;

    QueryPerformanceFrequency(&frequency);
    QueryPerformanceCounter(&start);

    return (start.QuadPart * 1000000)/frequency.QuadPart;
#else
    struct timespec start;
    clock_gettime(CLOCK_MONOTONIC, &start);
    
    return (start.tv_sec * 1000000LLU) + (start.tv_nsec / 1000);
#endif
}

class KortexController {
    private:
        mINI::INIStructure& config;
        SharedDataRobot& shared_data_robot;
        std::mutex& shared_data_robot_mutex;

        std::chrono::seconds action_timeout;
        k_api::Base::BaseClient* base;
        k_api::BaseCyclic::BaseCyclicClient* base_cyclic;
        k_api::ActuatorConfig::ActuatorConfigClient* actuator_config;
        k_api::SessionManager* session_manager;
        k_api::SessionManager* session_manager_real_time;
        k_api::RouterClient* router;
        k_api::RouterClient* router_real_time;
        k_api::TransportClientTcp* transport;
        k_api::TransportClientUdp* transport_real_time;
        double kp, ki, kd, joint_max_speed_deg;

    public:
    KortexController(mINI::INIStructure& config,
                SharedDataRobot& shared_data_robot,
                std::mutex& shared_data_robot_mutex)
    : config(config),
    shared_data_robot(shared_data_robot),
    shared_data_robot_mutex(shared_data_robot_mutex) {
        try {
            kp = std::stod(config["KortexController"]["Kp"]);
            ki = std::stod(config["KortexController"]["Ki"]);
            kd = std::stod(config["KortexController"]["Kd"]);
        }  catch (std::exception& e) {
            spdlog::error("KortexController: Unable to parse PID parameters: {}", e.what());
            throw std::invalid_argument("KortexController: PID parameters kp, ki, kd must be set");
        }
        try {
            joint_max_speed_deg = std::stod(config["KortexController"]["joint_max_speed_deg"]);
        }  catch (std::exception& e) {
            spdlog::error("KortexController: Unable to parse joint_max_speed_deg: {}", e.what());
            throw std::invalid_argument("KortexController: Maximal speed for joints must be set");
        }

        this->action_timeout = std::chrono::seconds(std::stoi(config["KortexController"]["action_timeout_sec"]));
        std::function error_callback = [](k_api::KError err){ spdlog::error("KortexController: API Error {}", err.toString()); };

        // Create API objects
        transport = new k_api::TransportClientTcp();
        router = new k_api::RouterClient(transport, error_callback);
        transport->connect(config["KortexController"]["ip"], std::stoi(config["KortexController"]["port"]));

        transport_real_time = new k_api::TransportClientUdp();
        router_real_time = new k_api::RouterClient(transport_real_time, error_callback);
        transport_real_time->connect(config["KortexController"]["ip"], std::stoi(config["KortexController"]["port_real_time"]));

        // Set session data connection information
        auto create_session_info = k_api::Session::CreateSessionInfo();
        create_session_info.set_username(config["KortexController"]["username"]);
        create_session_info.set_password(config["KortexController"]["password"]);
        create_session_info.set_session_inactivity_timeout(std::stoi(config["KortexController"]["session_inactivity_timeout_ms"]));   // (milliseconds)
        create_session_info.set_connection_inactivity_timeout(std::stoi(config["KortexController"]["connection_inactivity_timeout_ms"])); // (milliseconds)

        // Session manager service wrapper
        spdlog::info("KortexController: Creating sessions for communication");
        session_manager = new k_api::SessionManager(router);
        session_manager->CreateSession(create_session_info);
        session_manager_real_time = new k_api::SessionManager(router_real_time);
        session_manager_real_time->CreateSession(create_session_info);
        spdlog::info("KortexController: Sessions created");

        // Create services
        base = new k_api::Base::BaseClient(router);
        base_cyclic = new k_api::BaseCyclic::BaseCyclicClient(router_real_time);
        actuator_config = new k_api::ActuatorConfig::ActuatorConfigClient(router);
    }

    
    void operator()(std::stop_token should_run) {

        // Clearing faults
        try {
            base->ClearFaults();
        } catch(...) {
            spdlog::error("KortexController(): Unable to clean robot faluts");
            return;
        }

        k_api::BaseCyclic::Feedback base_feedback;
        k_api::BaseCyclic::Command  base_command;
        Eigen::Matrix<double,7,1> commands;
        Eigen::Matrix<double,7,1> velocity;
        Eigen::Matrix<double,7,1> joint_angles;

        auto servoingMode = k_api::Base::ServoingModeInformation();

        int64_t now = 0;
        int64_t last = 0;
        int timeout = 0;

        Eigen::Matrix<double,7,2> joint_forbidden_angles;
        (joint_forbidden_angles <<
        360., 360.,
        128.9, 360.-128.9,
        360., 360.,
        147.8, 360.-147.8,
        360., 360.,
        120.3, 360.-120.3,
        360., 360.
        ).finished();

        spdlog::info("KortexController(): Initializing the arm for velocity low-level control");
        try {
            // Set the base in low-level servoing mode
            servoingMode.set_servoing_mode(k_api::Base::ServoingMode::LOW_LEVEL_SERVOING);
            base->SetServoingMode(servoingMode);
            base_feedback = base_cyclic->RefreshFeedback();

            int actuator_count = base->GetActuatorCount().count();

            PID pid[] = {
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd),
                PID(.001, joint_max_speed_deg, -joint_max_speed_deg, kp, ki, kd)
            };
            // Initialize each actuator to its current position
            {
                std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                for (int i=0; i<actuator_count; i++) {
                    shared_data_robot.joint_angles(i) = base_feedback.actuators(i).position()*M_PI/180.;
                    shared_data_robot.goal_joint_angles(i) = base_feedback.actuators(i).position()*M_PI/180.;
                    base_command.add_actuators()->set_position(base_feedback.actuators(i).position());
                }
                shared_data_robot.joint_measurement_timestamp = std::chrono::high_resolution_clock::now();
            }

            // Define the callback function used in Refresh_callback
            auto lambda_fct_callback = [&](const Kinova::Api::Error &err, const k_api::BaseCyclic::Feedback data) {
                if (!should_run.stop_requested()) {
                    base_feedback = data;

                    //std::cout << "Jitter: ";
                    std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                    for (int i = 0; i < actuator_count; i++) {
                        shared_data_robot.joint_angles(i) = base_feedback.actuators(i).position()*M_PI/180.;
                        shared_data_robot.angular_velocities(i) = base_feedback.actuators(i).velocity()*M_PI/180.;
                        //std::cout << i << ": " <<  base_feedback.actuators(i).jitter_comm() << ","; // appears to be around 3620602532 us -> 1h20.6s, does not make sense
                    }
                    //std::cout << std::endl;
                    shared_data_robot.joint_measurement_timestamp = std::chrono::high_resolution_clock::now();
                    //std::cout << "x: " << base_feedback.interconnect().imu_acceleration_x() << ", y: " << base_feedback.interconnect().imu_acceleration_y() << ", z: " << base_feedback.interconnect().imu_acceleration_z() << std::endl;
                }
            };

            // Send a first frame
            base_feedback = base_cyclic->Refresh(base_command);

            // Set actuator in velocity mode now that the command is equal to measure
            auto control_mode_message = k_api::ActuatorConfig::ControlModeInformation();
            //control_mode_message.set_control_mode(k_api::ActuatorConfig::ControlMode::VELOCITY);
            // Device id == index + 1
            //for (int i=1; i<actuator_count+1; i++) {
            //    if (false) actuator_config->SetControlMode(control_mode_message, i);
            //}

            // Real-time loop
            while (!should_run.stop_requested()) {
                now = GetTickUs();
                if(now - last > 1000) {
                    {
                        std::lock_guard<std::mutex> lock(shared_data_robot_mutex);
                        if (shared_data_robot.goal_computed_timestamp.time_since_epoch().count() > 0.) {
                            commands = shared_data_robot.goal_joint_angles*180./M_PI; // Convert to degrees
                            shared_data_robot.joint_control_timestamp = std::chrono::high_resolution_clock::now();
                        }
                    }

                    bool write_base_command = true;
                    if (base_feedback.actuators(0).position() != 0. && !commands.isZero(1e-10)) { // Is disconnected
                        for (int i=0; i<actuator_count; i++) {
                            double goal_angle = fmod(commands(i), 360.);
                            if (goal_angle > joint_forbidden_angles(i,0) && goal_angle < joint_forbidden_angles(i,1)) {
                                write_base_command = false;
                                spdlog::error("KortexController(): Joint {} is set to: {}, which is inside the forbidden area: {}-{}", i, goal_angle, joint_forbidden_angles(i,0), joint_forbidden_angles(i,1));
                            }
                            double degree_error = angle_difference(base_feedback.actuators(i).position(), commands(i));
                            //if (abs(degree_error) < .1) commands(i) = base_feedback.actuators(i).position();
                            base_command.mutable_actuators(i)->set_position(goal_angle);
                            //std::cout << i << "=" << base_feedback.actuators(i).position() << "/" << fmod(commands(i), 360.) << "/" << round(degree_error*100.)/100. <<", ";
                            velocity(i) = pid[i].calculate(degree_error, 0.); // Inside the PID inputs are substracted, this is to wrap around 2pi
                            //double velocity = std::max({std::min({goal_velocity, joint_max_speed_deg}), -joint_max_speed_deg});
                            base_command.mutable_actuators(i)->set_velocity(velocity(i));
                            //std::cout << i << ": " << base_feedback.actuators(i).position() << "/" << fmod(commands(i), 360.) << " (" << round(degree_error*100.)/100. << ", "<< round(velocity*100.)/100. <<"), ";
                        }

                        //std::cout << std::endl;

                        // Incrementing identifier ensures actuators can reject out of time frames
                        base_command.set_frame_id(base_command.frame_id() + 1);
                        if (base_command.frame_id() > 65535) base_command.set_frame_id(0);

                        for (int idx = 0; idx < actuator_count; idx++) {
                            base_command.mutable_actuators(idx)->set_command_id(base_command.frame_id());
                        }

                    }
                    if (!write_base_command) {
                        spdlog::error("KortexController(): Joint angles outside joint limits, safety exit");
                        break;
                    }
                    try {
                        base_cyclic->Refresh_callback(base_command, lambda_fct_callback, 0);
                    } catch(...) {
                        timeout++;
                    }
                    
                    last = GetTickUs();
                }
            }

            // Set actuators back to position control
            control_mode_message.set_control_mode(k_api::ActuatorConfig::ControlMode::POSITION);
            for (int i=1; i<actuator_count+1; i++) {
                actuator_config->SetControlMode(control_mode_message, i);
            }
        } catch (k_api::KDetailedException& ex) {
            spdlog::error("KortexController(): API error: {}", ex.what());
        } catch (std::runtime_error& ex2) {
            spdlog::error("KortexController(): Runtime error: {}", ex2.what());
        }

        // Set back the servoing mode to Single Level Servoing
        servoingMode.set_servoing_mode(k_api::Base::ServoingMode::SINGLE_LEVEL_SERVOING);
        base->SetServoingMode(servoingMode);

        // Wait for a bit
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }

    bool goHome() {
        /*
        Robot goes to its home position.
        */

        // Make sure the arm is in Single Level Servoing before executing an Action
        auto servoingMode = k_api::Base::ServoingModeInformation();
        servoingMode.set_servoing_mode(k_api::Base::ServoingMode::SINGLE_LEVEL_SERVOING);
        base->SetServoingMode(servoingMode);
        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        
        // Move arm to ready position
        spdlog::info("KortexController.goHome: Moving the arm to a safe position");
        auto action_type = k_api::Base::RequestedActionType();
        action_type.set_action_type(k_api::Base::REACH_JOINT_ANGLES);
        auto action_list = base->ReadAllActions(action_type);
        auto action_handle = k_api::Base::ActionHandle();
        action_handle.set_identifier(0);
        for (auto action : action_list.action_list()) {
            if (action.name() == "Retract") {
                action_handle = action.handle();
            }
        }

        if (action_handle.identifier() == 0) {
            spdlog::error("KortexController.goHome: Can't reach safe position");
            return false;
        } else {
            bool action_finished = false; 
            // Notify of any action topic event
            auto options = k_api::Common::NotificationOptions();
            auto notification_handle = base->OnNotificationActionTopic(
                check_for_end_or_abort(action_finished),
                options
            );

            base->ExecuteActionFromReference(action_handle);

            while(!action_finished)
            { 
                std::this_thread::sleep_for(action_timeout);
            }

            base->Unsubscribe(notification_handle);
        }
        return true;
    }

    bool goPosition(Eigen::Vector3d linear = Eigen::Vector3d::Zero(), Eigen::Vector3d angular = Eigen::Vector3d::Zero()) {
        /* Robot goes to given position and orientation.
        If angular is not given, use current orientation.

        Args:
            linear (Eigen::Vector3d): x, y, z position in meters
            angular (Eigen::Vector3d): x, y, z orientation in degrees
        */
       auto feedback = base_cyclic->RefreshFeedback();

        auto action = k_api::Base::Action();
        action.set_name("Move to position");
        action.set_application_data("");

        auto constrained_pose = action.mutable_reach_pose();
        auto pose = constrained_pose->mutable_target_pose();
        pose->set_x(linear(0)); // x (meters)
        pose->set_y(linear(1)); // y (meters)
        pose->set_z(linear(2)); // z (meters)

        // If angular is not given, use current orientation
        if (angular == Eigen::Vector3d::Zero()) {
            pose->set_theta_x(feedback.base().tool_pose_x());
            pose->set_theta_y(feedback.base().tool_pose_y());
            pose->set_theta_z(feedback.base().tool_pose_z());
        } else {
            pose->set_theta_x(angular(0)); // theta x (degrees)
            pose->set_theta_y(angular(1)); // theta y (degrees)
            pose->set_theta_z(angular(2)); // theta z (degrees)
        }

        bool action_finished = false; 
        // Notify of any action topic event
        auto options = k_api::Common::NotificationOptions();
        auto notification_handle = base->OnNotificationActionTopic(
            check_for_end_or_abort(action_finished),
            options
        );

        base->ExecuteAction(action);

        while(!action_finished)
        { 
            std::this_thread::sleep_for(action_timeout);
        }

        base->Unsubscribe(notification_handle);
        return true;
    }

    void goContinuous(Eigen::Vector3d linear = Eigen::Vector3d::Zero(), Eigen::Vector3d angular = Eigen::Vector3d::Zero()) {
        /*
        Robot goes into continuous movement by setting velocity for each axis.
        To stop the robot, it is possible to call this without any argument / or use stop().

        Args:
            x, y, z (Eigen::Vector3d): Linear x, y, z-axis velocity (m/s)
            ax, ay, az (Eigen::Vector3d): Angular x, y, z-axis velocity (deg/s)
        */
        auto command = k_api::Base::TwistCommand();
        command.set_reference_frame(k_api::Common::CARTESIAN_REFERENCE_FRAME_BASE);
        command.set_duration(0);  // Unlimited time to execute

        auto twist = command.mutable_twist();
        twist->set_linear_x(linear.x());
        twist->set_linear_y(linear.y());
        twist->set_linear_z(linear.z());
        twist->set_angular_x(angular.x());
        twist->set_angular_y(angular.y());
        twist->set_angular_z(angular.z());
        base->SendTwistCommand(command);

        // Make movement stop if no position movement
        if (linear.isZero(1e-5) && angular.isZero(1e-5)) base->Stop();
    }

    void stop() {
        base->Stop();
    }

    void goGripper(float speed = 0.0f) {
        auto command = k_api::Base::GripperCommand();
        command.set_mode(k_api::Base::GRIPPER_SPEED);
        
        auto finger = command.mutable_gripper()->add_finger();
        finger->set_finger_identifier(1);
        finger->set_value(speed);

        base->SendGripperCommand(command);
    }

    Eigen::Vector3d feedback() {
        k_api::Base::Pose pose = base->GetMeasuredCartesianPose();
        Eigen::Vector3d coordinates(pose.x(), pose.y(), pose.z());
        return coordinates;
    }

    Eigen::Matrix<double,7,1> get_joint_angles() {
        Eigen::Matrix<double,7,1> joint_angles_res;
        k_api::Base::JointAngles joint_angles = base->GetMeasuredJointAngles();
        //std::cout << "Joint ID : Joint Angle" << std::endl;
        for (auto joint_angle : joint_angles.joint_angles()) {
            //std::cout << joint_angle.joint_identifier() << " : " << joint_angle.value()*(M_PI/180.) << std::endl;
            joint_angles_res(joint_angle.joint_identifier()) = joint_angle.value()*(M_PI/180.);
        }
        return joint_angles_res;
    }

    Eigen::Matrix4d full_feedback() {
        k_api::Base::Pose pose = base->GetMeasuredCartesianPose();
        Eigen::AngleAxisd xAngle(pose.theta_x()*M_PI/180., Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd yAngle(pose.theta_y()*M_PI/180., Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd zAngle(pose.theta_z()*M_PI/180., Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> q = zAngle * yAngle * xAngle;
        Eigen::Matrix3d rotationMatrix = q.matrix();
        Eigen::Vector3d coordinates(pose.x(), pose.y(), pose.z());

        Eigen::Matrix4d T;
        T(3,3) = 1.;
        for (int i=0; i<3; i++) {
            for (int j=0; j<3; j++) {
                T(i,j) = rotationMatrix(i,j);
            }
            T(i,3) = coordinates(i);
        }

        return T;
    }

    Eigen::Matrix3d feedbackRotation() {
        k_api::Base::Pose pose = base->GetMeasuredCartesianPose();
        Eigen::AngleAxisd xAngle(pose.theta_x()*M_PI/180., Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd yAngle(pose.theta_y()*M_PI/180., Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd zAngle(pose.theta_z()*M_PI/180., Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> q = zAngle * yAngle * xAngle;
        Eigen::Matrix3d rotationMatrix = q.matrix();
        return rotationMatrix;
    }



    ~KortexController() {
        // Close API session
        session_manager_real_time->CloseSession();
        session_manager->CloseSession();

        // Deactivate the router and disconnect from the transport object
        router->SetActivationStatus(false);
        transport->disconnect();
        router_real_time->SetActivationStatus(false);
        transport_real_time->disconnect();
        
        // Destroy the API
        delete base;
        delete base_cyclic;
        delete actuator_config;
        delete session_manager;
        delete session_manager_real_time;
        delete router;
        delete router_real_time;
        delete transport;
        delete transport_real_time;
    }
};

#endif // KORTEX_CONTROLLER_H
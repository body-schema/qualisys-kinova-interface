#ifndef CAMERA_READER_H
#define CAMERA_READER_H

#pragma once

#include <string>
#include <iostream>
#include "spdlog/spdlog.h"
#include "RTProtocol.h"
#include "RTPacket.h"

class CameraReader {
private:
    mINI::INIStructure& config;
    SharedDataCamera& shared_data_camera;
    std::mutex& shared_data_camera_mutex;
    SharedDataUi& shared_data_ui;
    std::mutex& shared_data_ui_mutex;
public:
    CameraReader(mINI::INIStructure& config,
             SharedDataCamera& shared_data_camera,
             std::mutex& shared_data_camera_mutex,
             SharedDataUi& shared_data_ui,
             std::mutex& shared_data_ui_mutex)
    : config(config),
      shared_data_camera(shared_data_camera),
      shared_data_camera_mutex(shared_data_camera_mutex),
      shared_data_ui(shared_data_ui),
      shared_data_ui_mutex(shared_data_ui_mutex) {
    }

    void operator()(std::stop_token should_run) {
        while (!should_run.stop_requested()) {
            try {
                CRTProtocol rtProtocol;

                bool dataAvailable = false;
                bool streamFrames = false;
                unsigned short udpPort = 0;
                
                while (!should_run.stop_requested()) {
                    while (!rtProtocol.Connected() && !should_run.stop_requested()) {
                        if (!rtProtocol.Connect(
                                config["CameraReader"]["ip"].c_str(),
                                std::stoi(config["CameraReader"]["port"]),
                                &udpPort,
                                std::stoi(config["CameraReader"]["versionMajor"]),
                                std::stoi(config["CameraReader"]["versionMinor"]),
                                std::stoi(config["CameraReader"]["bigEndian"])
                            )
                        ) {
                            spdlog::error("CameraReader(): rtProtocol.Connect {}", rtProtocol.GetErrorString());
                            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                            continue;
                        } else {
                            while (!rtProtocol.Connected() && !should_run.stop_requested()) {
                                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                            }
                            if (!rtProtocol.TakeControl(config["CameraReader"]["password"].c_str())) {
                                spdlog::error("CameraReader(): rtProtocol.TakeControl {}", rtProtocol.GetErrorString());
                            }

                            while (!rtProtocol.IsControlling() && !should_run.stop_requested()) {
                                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                            }

                            while (!rtProtocol.NewMeasurement() && !should_run.stop_requested()) {
                                spdlog::error("CameraReader(): rtProtocol.NewMeasurement '{}'", rtProtocol.GetErrorString());
                                //if (strcmp(rtProtocol.GetErrorString(), "The previous measurement has not been saved or closed.")) {
                                    char new_filename[300];
                                    while (!rtProtocol.SaveCapture("first_pilots.qtm", false, new_filename, sizeof(new_filename))) {
                                        std::this_thread::sleep_for(std::chrono::seconds(1));
                                    }
                                    if (strlen(new_filename) == 0) {
                                        spdlog::info("CameraReader(): measurement saved as first_pilots.qtm");
                                    } else {
                                        spdlog::info("CameraReader(): measurement saved as {}", new_filename);
                                    }
                                    rtProtocol.CloseMeasurement();
                                //}
                                //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                            }

                            while (!rtProtocol.StartCapture() && !should_run.stop_requested()) {
                                spdlog::error("CameraReader(): rtProtocol.StartCapture {}", rtProtocol.GetErrorString());
                                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                            }
                            spdlog::info("CameraReader(): started recording");
                        }
                    }

                    bool event_trigger = false;
                    std::string event_message;
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        event_trigger = shared_data_ui.event_trigger;
                        event_message = shared_data_ui.event_message;
                    }
                    if (event_trigger) {
                        if (rtProtocol.SetQTMEvent(event_message.c_str())) {
                            std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                            shared_data_ui.event_trigger = false;
                        } else {
                            spdlog::error("CameraReader(): rtProtocol.SetQTMEvent {}", rtProtocol.GetErrorString());
                        }
                    }

                    if (!dataAvailable) {
                        if (!rtProtocol.Read3DSettings(dataAvailable))
                        {
                            spdlog::error("CameraReader(): rtProtocol.Read3DSettings {}", rtProtocol.GetErrorString());
                            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                            continue;
                        }
                    }

                    if (!streamFrames) {
                        if (!rtProtocol.StreamFrames(CRTProtocol::RateAllFrames, 0, 0, nullptr, CRTProtocol::cComponent3d)) {
                            spdlog::error("CameraReader(): rtProtocol.StreamFrames {}", rtProtocol.GetErrorString());
                            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                            continue;
                        }
                        streamFrames = true;

                        spdlog::info("CameraReader(): Starting to streaming 3D data");
                    }

                    CRTPacket::EPacketType packetType;
                    if (rtProtocol.Receive(packetType, true) == CNetwork::ResponseType::success) {
                        if (packetType == CRTPacket::PacketData) {
                            float fX, fY, fZ;

                            CRTPacket* rtPacket = rtProtocol.GetRTPacket();

                            auto t_now = std::chrono::high_resolution_clock::now();
                            if (rtPacket->Get3DMarkerCount() > 0) {
                                for (unsigned int i = 0; i < rtPacket->Get3DMarkerCount(); i++) {
                                    const char* tmpStr = rtProtocol.Get3DLabelName(i);

                                    if (rtPacket->Get3DMarker(i, fX, fY, fZ)) {
                                        // Convert everything to meters (SI units used everywhere)
                                        fX = fX/1000.0;
                                        fY = fY/1000.0;
                                        fZ = fZ/1000.0;
                                        Eigen::Vector3d marker_pos = Eigen::Vector3d(fX, fY, fZ);
                                        std::chrono::duration<double> diff = t_now - shared_data_camera.hand_measurement_timestamp;

                                        if (strcmp(tmpStr, "hand_end") == 0) {

                                            const std::lock_guard<std::mutex> lock(shared_data_camera_mutex);                                            
                                            shared_data_camera.hand_vel = (shared_data_camera.hand_pos - marker_pos)/diff.count();
                                            shared_data_camera.hand_pos = marker_pos;
                                            shared_data_camera.hand_measurement_timestamp = t_now;
                                        }
                                        if (strcmp(tmpStr, "elbow") == 0) {
                                            const std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                                            shared_data_camera.elbow_pos = marker_pos;
                                            shared_data_camera.elbow_measurement_timestamp = t_now;
                                        }
                                        if (strcmp(tmpStr, "hand_left") == 0) {
                                            const std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                                            shared_data_camera.hand_left_pos = marker_pos;
                                            shared_data_camera.hand_left_measurement_timestamp = t_now;
                                        }
                                        if (strcmp(tmpStr, "robot_end") == 0) {
                                            const std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
                                            shared_data_camera.robot_vel = (shared_data_camera.robot_pos - marker_pos)/diff.count();
                                            shared_data_camera.robot_pos = marker_pos;
                                            shared_data_camera.robot_measurement_timestamp = t_now;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (rtProtocol.Connected()) {
                    // Final saving, disconnect etc.
                    while (rtProtocol.StopCapture()) {
                        std::this_thread::sleep_for(std::chrono::seconds(1));
                    }
                    spdlog::info("CameraReader(): Stoped capturing");

                    int participant_id;
                    {
                        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
                        participant_id = shared_data_ui.participant_id;
                    }

                    char new_filename[300];
                    std::string str = "participant_id_"+ std::to_string(participant_id) +".qtm";
                    const char* filename = str.c_str();
                    while (!rtProtocol.SaveCapture(filename, false, new_filename, sizeof(new_filename))) {
                        std::cout << "CameraReader(): rtProtocol.SaveCapture " << rtProtocol.GetErrorString() << std::endl;
                        std::this_thread::sleep_for(std::chrono::seconds(1));
                    }
                    if (strlen(new_filename) == 0) {
                        spdlog::info("CameraReader(): measurement saved as " + str);
                    } else {
                        spdlog::info("CameraReader(): measurement saved as {}", new_filename);
                    }

                    rtProtocol.CloseMeasurement();
                    spdlog::info("CameraReader(): Measurement closed");

                    rtProtocol.ReleaseControl();
                    spdlog::info("CameraReader(): Released control");

                    rtProtocol.Disconnect();
                    spdlog::info("CameraReader(): disconnected");
                }
                spdlog::info("CameraReader(): exitting");
            } catch (std::exception& e) {
                spdlog::error("CameraReader(): {}", e.what());
            }
        }
    }
};

#endif // CAMERA_READER_H
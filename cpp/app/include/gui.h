#ifndef GUI_WINDOW_H
#define GUI_WINDOW_H

#pragma once

#include <wx/sizer.h>
#include <wx/display.h>
#include <wx/settings.h>
#include <wx/wx.h>
#include <wx/timer.h>
#include <thread>
#include <mutex>
#include <utility>
#include <random>

#include "spdlog/spdlog.h"
#include "shared_memory.h"
#include "ini.h"
#include "camera_reader.h"
#include "robot_controller.h"
#include "ui.h"

class BasicDrawPane;

class RenderTimer : public wxTimer {
    BasicDrawPane* pane;
public:
    RenderTimer(BasicDrawPane* pane);
    void Notify();
    void start();
};

class BasicDrawPane : public wxPanel {
    private:
        mINI::INIStructure& config;
        SharedDataCamera& shared_data_camera;
        std::mutex& shared_data_camera_mutex;
        SharedDataUi& shared_data_ui;
        std::mutex& shared_data_ui_mutex;
        bool isController;
    public:
        BasicDrawPane(wxFrame* parent,
            bool isController,
            mINI::INIStructure& config,
            SharedDataCamera& shared_data_camera,
            std::mutex& shared_data_camera_mutex,
            SharedDataUi& shared_data_ui,
            std::mutex& shared_data_ui_mutex);
        
        void paintEvent(wxPaintEvent & evt);
        void paintNow();
        
        void render(wxDC& dc);
        // some useful events
        /*
        void mouseMoved(wxMouseEvent& event);
        void mouseDown(wxMouseEvent& event);
        void mouseWheelMoved(wxMouseEvent& event);
        void mouseReleased(wxMouseEvent& event);
        void mouseLeftWindow(wxMouseEvent& event);
        void keyPressed(wxKeyEvent& event);
        void keyReleased(wxKeyEvent& event);
        */
        
        void rightClick(wxMouseEvent& event);
        void keyPressed(wxKeyEvent& event);
        DECLARE_EVENT_TABLE()
};


RenderTimer::RenderTimer(BasicDrawPane* pane) : wxTimer()
{
    RenderTimer::pane = pane;
}

void RenderTimer::Notify()
{
    pane->Refresh();
}

void RenderTimer::start()
{
    wxTimer::Start(1);
}

class MyFrame;
class GuiApp: public wxApp {
    bool OnInit();
    int OnExit();
    void OnTimer(wxTimerEvent& event);
    
    MyFrame *frame_experiment;
    //MyFrame *frame_controller;
    std::jthread cameraThread;
    std::jthread robotThread;
    std::jthread uiThread;

    Ui *ui;

    // initialize shared variables
    mINI::INIStructure config;
    SharedDataCamera shared_data_camera;
    std::mutex shared_data_camera_mutex;
    SharedDataUi shared_data_ui;
    std::mutex shared_data_ui_mutex;
    wxTimer ui_timer;
public:
};

//IMPLEMENT_APP(GuiApp)

double CM_TO_PX;
class MyFrame : public wxFrame
{
    RenderTimer* timer;
    BasicDrawPane* drawPane;
    mINI::INIStructure& config;
    SharedDataCamera& shared_data_camera;
    std::mutex& shared_data_camera_mutex;
    SharedDataUi& shared_data_ui;
    std::mutex& shared_data_ui_mutex;
    
public:
    MyFrame(bool isController,
            const wxString& title,
            mINI::INIStructure& config,
            SharedDataCamera& shared_data_camera,
            std::mutex& shared_data_camera_mutex,
            SharedDataUi& shared_data_ui,
            std::mutex& shared_data_ui_mutex)
    : wxFrame((wxFrame *)NULL, -1, title, wxDefaultPosition, wxSize(400,200), wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxRESIZE_BORDER | wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN | wxFRAME_EX_METAL),
    shared_data_camera(shared_data_camera),
    config(config),
    shared_data_camera_mutex(shared_data_camera_mutex),
    shared_data_ui(shared_data_ui),
    shared_data_ui_mutex(shared_data_ui_mutex) {
        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        drawPane = new BasicDrawPane( this, isController, config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex );
        sizer->Add(drawPane, 1, wxEXPAND);
        SetSizer(sizer);
        SetAutoLayout(true);
        
        timer = new RenderTimer(drawPane);
        if (!isController) {
            drawPane->SetBackgroundColour(wxColor(220, 220, 220));
        }
        Show();
        drawPane->paintNow();
        timer->start();
    }

    ~MyFrame()
    {
        delete timer;
    }
    void onClose(wxCloseEvent& evt)
    {
        timer->Stop();
        evt.Skip();
    }
};

bool GuiApp::OnInit() {
    // read config
    mINI::INIFile configFile("../config/settings.ini");
    configFile.read(config);

    if (argc != 2) {
        spdlog::error("GuiApp.OnInit: Please specify the ID of the participant");
        return false;
    }

    try {
        double SCREEN_HEIGHT_CM = std::stod(config["Gui"]["screen_height_cm"]);
        double SCREEN_HEIGHT_PX = std::stod(config["Gui"]["screen_height_px"]);
        double SCREEN_PX_DIVIDER = std::stod(config["Gui"]["screen_px_divider"]);
        CM_TO_PX = SCREEN_HEIGHT_PX / (SCREEN_HEIGHT_CM * SCREEN_PX_DIVIDER);

    }  catch (std::exception& e) {
        spdlog::error("GuiApp.OnInit: Unable to parse screen_height_cm, screen_height_px, screen_px_divider: {}", e.what());
        throw std::invalid_argument("GuiApp.OnInit: Screen height in centimeters, pixels and pixels divider must be set");
    }

    int participant_id;
    try {
        participant_id = std::stoi(argv[1].ToStdString());
        spdlog::info("GuiApp.OnInit: Participant id: {}", participant_id);
    } catch (std::exception& e) {
        spdlog::error("GuiApp.OnInit: Unable to parse participant id: {}", e.what());
        throw std::invalid_argument("GuiApp.OnInit: Participant id must be an integer");
    }

    {
        std::lock_guard<std::mutex> lock(shared_data_camera_mutex);
        shared_data_camera = SharedDataCamera();
    }

    {
        std::lock_guard<std::mutex> lock(shared_data_ui_mutex);
        shared_data_ui = SharedDataUi();
        shared_data_ui.control_input = 0;
        shared_data_ui.participant_id = participant_id;
    }
 
    // initialize camera reader thread - reads data from cameras
    CameraReader cameraReader(config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex);
    cameraThread = std::jthread(cameraReader);
    spdlog::debug("GuiApp.OnInit: Starting CameraThread");

    // initialize robot controller thread - controlls the robot + spawns low-level controller thread
    RobotController robotController(config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex);
    robotThread = std::jthread(robotController);
    spdlog::debug("GuiApp.OnInit: Starting RobotThread");

    // initialize ui thread - gathers user input from console
    ui = new Ui(config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex);
    uiThread = std::jthread(*ui);
    spdlog::debug("GuiApp.OnInit: Starting UiThread");

    ui_timer.SetOwner(this);
    ui_timer.Start(50); // 50ms
    Bind(wxEVT_TIMER, &GuiApp::OnTimer, this, ui_timer.GetId());

    frame_experiment = new MyFrame(false, wxT("Experiment"), config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex);
    //frame_controller = new MyFrame(true, wxT("Controller"), config, shared_data_camera, shared_data_camera_mutex, shared_data_ui, shared_data_ui_mutex);
    frame_experiment->Show();
    //frame_controller->Show();

    return true;
}

int GuiApp::OnExit() {
        cameraThread.request_stop();
        robotThread.request_stop();
        uiThread.request_stop();
        cameraThread.join();
        robotThread.join();
        uiThread.join();

        return 0;
}

void GuiApp::OnTimer(wxTimerEvent& event) {
        if (!ui->update()) {
            ui_timer.Stop();
            ExitMainLoop();
        }
}

BEGIN_EVENT_TABLE(BasicDrawPane, wxPanel)
    // some useful events
    /*
    EVT_MOTION(BasicDrawPane::mouseMoved)
    EVT_LEFT_DOWN(BasicDrawPane::mouseDown)
    EVT_LEFT_UP(BasicDrawPane::mouseReleased)
    EVT_LEAVE_WINDOW(BasicDrawPane::mouseLeftWindow)
    EVT_KEY_DOWN(BasicDrawPane::keyPressed)
    EVT_KEY_UP(BasicDrawPane::keyReleased)
    EVT_MOUSEWHEEL(BasicDrawPane::mouseWheelMoved)
    */
    EVT_RIGHT_DOWN(BasicDrawPane::rightClick)
    EVT_KEY_DOWN(BasicDrawPane::keyPressed)
    // catch paint events
    EVT_PAINT(BasicDrawPane::paintEvent)
END_EVENT_TABLE()


// some useful events
/*
 void BasicDrawPane::mouseMoved(wxMouseEvent& event) {}
 void BasicDrawPane::mouseDown(wxMouseEvent& event) {}
 void BasicDrawPane::mouseWheelMoved(wxMouseEvent& event) {}
 void BasicDrawPane::mouseReleased(wxMouseEvent& event) {}
 void BasicDrawPane::mouseLeftWindow(wxMouseEvent& event) {}
 void BasicDrawPane::keyPressed(wxKeyEvent& event) {}
 void BasicDrawPane::keyReleased(wxKeyEvent& event) {}
 */

 void BasicDrawPane::rightClick(wxMouseEvent& event) {
    {
        std::lock_guard<std::mutex> lock(this->shared_data_ui_mutex);
        if (this->shared_data_ui.control_input == 0) {
            this->shared_data_ui.control_input = 'b';
        }
    }
 }

 void BasicDrawPane::keyPressed(wxKeyEvent& event) {
    int c = event.GetKeyCode();
    if (c == 43 || c == 325) c = '1';
    if (c == 492 || c == 326) c = '2';
    if (c == 441 || c == 327) c = '3';
    if (c == 504 || c == 329) c = '5';
    if (c == 446 || c == 330) c = '6';
    if (c == 388) c = '+';
    if (c == 390) c = '-';
    {
        std::lock_guard<std::mutex> lock(this->shared_data_ui_mutex);
        if (this->shared_data_ui.control_input == 0 && c < 255) {
            this->shared_data_ui.control_input = c; //wxAcceleratorEntry(0, event.GetUnicodeKey()).ToString()[0];
        }
    }
 }

BasicDrawPane::BasicDrawPane(wxFrame* parent,
        bool isController,
        mINI::INIStructure& config,
        SharedDataCamera& shared_data_camera,
        std::mutex& shared_data_camera_mutex,
        SharedDataUi& shared_data_ui,
        std::mutex& shared_data_ui_mutex)
    : wxPanel(parent),
    isController(isController),
    config(config),
    shared_data_camera(shared_data_camera),
    shared_data_camera_mutex(shared_data_camera_mutex),
    shared_data_ui(shared_data_ui),
    shared_data_ui_mutex(shared_data_ui_mutex) {
}

/*
 * Called by the system of by wxWidgets when the panel needs
 * to be redrawn. You can also trigger this call by
 * calling Refresh()/Update().
 */

void BasicDrawPane::paintEvent(wxPaintEvent & evt)
{
    wxPaintDC dc(this);
    render(dc);
}

/*
 * Alternatively, you can use a clientDC to paint on the panel
 * at any time. Using this generally does not free you from
 * catching paint events, since it is possible that e.g. the window
 * manager throws away your drawing when the window comes to the
 * background, and expects you will redraw it when the window comes
 * back (by sending a paint event).
 *
 * In most cases, this will not be needed at all; simply handling
 * paint events and calling Refresh() when a refresh is needed
 * will do the job.
 */
void BasicDrawPane::paintNow() {
    wxClientDC dc(this);
    render(dc);
}

/*
 * Here we do the actual rendering. I put it in a separate
 * method so that it can work no matter what type of DC
 * (e.g. wxPaintDC or wxClientDC) is used.
 */
void BasicDrawPane::render(wxDC& dc) {
    SCENE_TYPE scene;
    std::chrono::high_resolution_clock::time_point scene_change_timestamp;
    VARIANT_TYPE variant;
    std::chrono::high_resolution_clock::time_point variant_change_timestamp;

    int trial = 1;
    int total_trials = 1;

    double distance_cm = 0.;
    {
        std::lock_guard<std::mutex> lock(this->shared_data_ui_mutex);
        scene = this->shared_data_ui.scene;
        scene_change_timestamp = this->shared_data_ui.scene_change_timestamp;
        variant = this->shared_data_ui.variant;
        variant_change_timestamp = this->shared_data_ui.variant_change_timestamp;

        trial = this->shared_data_ui.trial;
        total_trials = this->shared_data_ui.total_trials;

        distance_cm = this->shared_data_ui.distance_cm;
    }

    bool show_center_text = false;
    wxString center_text = wxT("");

    bool show_line = false;
    bool line_left = true; // true = left, false = right

    bool show_trial_number = false;

    auto t_now = std::chrono::high_resolution_clock::now();
    double scene_change_sec = std::chrono::duration<double>(t_now - scene_change_timestamp).count();

    switch(scene) {
        case SCENE_INSTRUCTION:
            switch(variant) {
                case VARIANT_FAMILIARIZATION:
                    center_text = wxT("Sezn\u00E1men\u00ED s experimentem,\nklikn\u011Bte pro zah\u00E1jen\u00ED.");
                    show_center_text = true;
                    break;
                case VARIANT_TOUCH:
                    center_text = wxT("Klikn\u011Bte pro zah\u00E1jen\u00ED,\nsoust\u0159ed\u00EDte se na pohyb pravé ruky.");
                case VARIANT_MOVEMENT:
                    if (variant == VARIANT_MOVEMENT) center_text = wxT("Klikn\u011Bte pro zah\u00E1jen\u00ED,nsoust\u0159ed\u00EDte se na pohyb levé ruky.");
                    show_center_text = true;
                    show_trial_number = true;
                    break;
                default:
                    spdlog::debug("GuiApp.render: Unknown combination scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
                    break;
            }
            break;
        case SCENE_PUSH_TO_START:
            switch(variant) {
                case VARIANT_TOUCH:
                case VARIANT_MOVEMENT:
                    center_text = wxT("Klikn\u011Bte pro zah\u00E1jen\u00ED");
                    show_center_text = true;
                    show_trial_number = true;
                    break;
                default:
                    spdlog::debug("GuiApp.render: Unknown combination scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
                    break;
            }
            break;
        case SCENE_321:
            show_center_text = true;
            if (scene_change_sec < 1.) {
                center_text = wxT("3");
            } else if (scene_change_sec < 2.) {
                center_text = wxT("2");
            } else if (scene_change_sec < 3.) {
                center_text = wxT("1");
            } else {
                center_text = wxT("+");
            }
            break;
        case SCENE_JUDGEMENT:
            switch(variant) {
                case VARIANT_TOUCH:
                    line_left = false;
                case VARIANT_MOVEMENT:
                    show_line = true;
                    break;
                default:
                    spdlog::debug("GuiApp.render: Unknown combination scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
                    break;
            }
            break;
        default:
            spdlog::debug("GuiApp.render: Unknown scene " + std::to_string(scene) + " with variant " + std::to_string(variant));
            break;
    }

    // draw some text
    wxFont font = dc.GetFont();
    dc.SetTextForeground(wxColour(150, 150, 150));
    dc.SetFont(font.Scale(3));

    // create a pen
    wxColour grayColor(150, 150, 150);
    wxBrush brush(grayColor);
    //wxBrush brush = (*wxBLACK_BRUSH); //pen(*wxWHITE, 60);
    dc.SetBrush(brush);

    int window_width, window_height;
    dc.GetSize(&window_width, &window_height);

    // x from left, y from top
    if (show_center_text) {
        // Get the size of the text
        wxSize textSize = dc.GetTextExtent(center_text);

        // Calculate the position to center the text
        int x_centered = (window_width - textSize.GetWidth()) / 2;
        int y_centered = (window_height - textSize.GetHeight()) / 2;
        dc.DrawText(center_text, x_centered, y_centered);
    }

    if (show_line) {
        int hand_width = static_cast<int>(2. * CM_TO_PX); // 6 (5.7) cm, 2 = .3 cm -> 1px = .15 cm
        int line_x = line_left ? window_width/3 : window_width*2/3 - hand_width;
        int distance_px = static_cast<int>(distance_cm * CM_TO_PX);
        int line_y = (window_height - distance_px) / 2;
        dc.DrawRectangle(line_x, line_y, hand_width, distance_px);
    }

    if (show_trial_number) {
        dc.DrawText(std::to_string(trial) + "/" + std::to_string(total_trials), window_width*7/8, window_height/8);
    }
}

#endif // GUI_WINDOW_H
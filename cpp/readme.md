# Teleoperation of Kinova3 Robot using Qualisys Motion Cameras
# C++ Version

This project aims to provide teleoperation capabilities for the Kinova3 robot using Qualisys motion cameras for tracking human motion. The project structure is as follows:

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

- Kinova3 robot connected to the local network
- Qualisys Motion Tracking with the cameras ready
- Docker
- Docker Compose
- Xquartz (on MacOS)

### Installing

1. Clone the repository onto your local machine:
```
git clone https://gitlab.fel.cvut.cz/body-schema/qualisys-kinova-interface.git
```


2. Build the Docker containers using the `docker compose` command:
```
cd cpp
docker compose build
```

3. Make sure that you use the correct settings in app/config/settings.ini

4. Before running the project, have the robot and Qualisys Motion Capture laptop ready with the correct AIM model and calibration.


### Running the project

0. Enable xhost (MacOS)
```
export DISPLAY=:0
/opt/X11/bin/xhost +
```

1. Start the Docker containers using the `docker compose` command:
```
docker compose run kinova_cpp
cmake ..
cmake --build .
./QualisysKinovaInterface
```

2. The project should now be running and you should see the robot moving in accordance with the motion captured by the cameras.

## Project Structure

### app/CMakeLists.txt
This file contains the CMake configuration for building the project.

### app/config/settings.ini
This file contains the settings for the IP addresses of the cameras and the robot, as well as other parameters.

### app/include/
This folder contains the header files for the project.

### app/src/
This folder contains the source files for the project.

### app/src/main.cpp
This file contains the main loop that spawns separate threads for reading the camera data and controlling the robot. It also initializes the shared memory and reads the settings from the settings.ini file.

### app/src/camera_reader.h
This file is responsible for reading data from the Qualisys motion cameras and sending it to a shared memory.

### app/src/kortex_controller.h
This file contains the functions for sending commands to the Kinova3 robot using the Kortex API.

### app/src/robot_controller.h
This file contains high level commands for controlling the robot.

### app/src/shared_memory.h
This file is a support file that provides a class for sharing memory across multiple threads.

### lib/qualisys-1.23/
This folder contains the Qualisys Motion Capture library.

### lib/kortex-2.5.0/
This folder contains the Kortex API library.

### entrypoint.sh
This file is used as ~/.bashrc in the Docker container to link libraries correctly.

### Dockerfile
This file contains the configuration for building the Docker container.

### compose.yml
This file contains the Docker Compose configuration for running the Docker container.

## Built With

* [Kortex API](https://github.com/Kinovarobotics/kortex) - Used for communication with the Kinova3 robot
* [Qualisys Motion Capture](https://www.qualisys.com/) - Used for capturing 3D keypoints used by the AIM model

## Authors

* **Adam Rojík** - [Author's Github](https://github.com/rojikada)


# Teleoperation of Kinova3 Robot using Qualisys Motion Cameras
# Warning - Python version deprecated due to latency limitations. Please refer to C++ version.

This project aims to provide teleoperation capabilities for the Kinova3 robot using Qualisys motion cameras for tracking human motion. The project structure is as follows:

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

- Kinova3 robot connected to the local network
- Qualisys Motion Tracking with the cameras ready
- Python 3.8

### Installing

1. Clone the repository onto your local machine:
```git clone https://gitlab.fel.cvut.cz/body-schema/qualisys-kinova-interface.git```

2. Install required python libraries
```pip install -r requirements.txt```

3. Update the settings in the `settings.ini` file with the appropriate IP addresses and other parameters.

### Running the project

1. Start the main script 
```python main.py```

2. The project should now be running and you should see the robot moving in accordance with the motion captured by the cameras.

## Project Structure

### main.py
This file contains the main loop that spawns separate processes for reading the camera data and controlling the robot. It also initializes the shared memory and reads the settings from the settings.ini file.

### camera_reader.py
This file is responsible for reading data from the Qualisys motion cameras and sending it to a shared memory.

### kortex_controller.py
This file contains the functions for sending commands to the Kinova3 robot using the Kortex API.

### robot_controller.py
This file contains high level commands for controlling the robot.

### settings.ini
This file contains the settings for the IP addresses of the cameras and the robot, as well as other parameters.

### shared_memory.py
This file is a support file that provides a class for sharing memory across multiple processes.


## Built With

* [Kortex API](https://github.com/Kinovarobotics/kortex) - Used for communication with the Kinova3 robot
* [Qualisys Motion Capture](https://www.qualisys.com/) - Used for capturing 3D positions


## Authors

* **Adam Rojík** - *Initial work* - [Author's Github](https://github.com/rojikada)

## License

This project is licensed under the [License](link) - see the [LICENSE.md](link) file for details

## Acknowledgments

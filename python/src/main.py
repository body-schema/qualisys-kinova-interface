from multiprocessing import Process, Manager
from shared_memory import SharedMemoryManager
import logging
import time

from camera_reader import CameraReader
# from transformer import Transformer
from robot_controller import RobotController
# from controller_loop import ControllerLoop
# from visual_gui import VisualGui

from multiprocessing import Manager
from shared_memory import SharedMemoryManager
from support import config

logger = logging.getLogger(__name__)

logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())

initial_variables = {
    'x_goal': 0.,
    'y_goal': 0.,
    'z_goal': 0.,
    'goal': [0., 0., 0.],
    'robot_position_c': [0., 0., 0.],

    'last_timestamp': 0.,
    'y_window': [0., 0., 0., 0., 0.],
    'y_velocity': [0., 0., 0., 0., 0.],
    'window_timestamps': [0., 0., 0., 0., 0.],
    'window_index': 0,
    'shouldRun': True,
}

if __name__ == '__main__':
    with Manager() as manager:
        with SharedMemoryManager(manager, variables=initial_variables) as shared_memory:
            cr = CameraReader(config['CameraReader'], shared_memory.vars)
            rc = RobotController(config['RobotController'], shared_memory.vars)

            p1 = Process(target=cr.run)
            logger.info("will run RC")
            p2 = Process(target=rc.run)

            p1.start()
            p2.start()
            logger.info("RC should be running")

            time.sleep(150)
            shared_memory.vars['shouldRun'] = False

            p1.join()
            p2.join()

    # TODO : read camera data, put them to shared memory
    # TODO : from shared memory read camera data and write them to different shm, transformed
    # TODO : from shared memory read transformed coords and send them to robot

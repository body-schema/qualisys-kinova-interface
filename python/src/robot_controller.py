import logging
from utilities import obj2np
#, PID

import numpy as np
import time
import json
import kortex_utilities as utilities
from kortex_controller import KortexController
from rigid_transform_3D import rigid_transform_3D

logger = logging.getLogger(__name__)


class RobotController(object):
    def __init__(self, config, shared_variables):
        self.vars = shared_variables
        self.config = config
        self.kortex = None

        self.transformation = None
        if self.config["transformation"] != "":
            try:
                self.transformation = json.loads(self.config["transformation"])
                self.transformation[0] = np.array(self.transformation[0])
                self.transformation[0].shape = (3, 3)
                self.transformation[1] = np.array(self.transformation[1])
                self.transformation[1].shape = (3, 1)
                logger.info(f'RobotController: using calibration from settings.')
            except Exception as e:
                logger.warning(f'RobotController: invalid transformation in settings.ini ({e})')
                self.transformation = None

    def init(self):
        if self.kortex is None:
            raise ConnectionError("RobotController: kortex not initialized during init phase.")

        logger.info('RobotController: robot going home.')
        self.kortex.goHome(True)

        if self.transformation is None:
            self.calibration()

    def calibration(self):
        if self.kortex is None:
            raise ConnectionError("RobotController: kortex not initialized during calibration phase.")

        logger.info('RobotController: Waiting for robot position from camera.')
        while np.isnan(np.array(self.vars['robot_position_c'])).all():
            time.sleep(.1)

        logger.info('RobotController: running calibration.')
        camera_set = []
        robot_set = []

        camera_set.append(self.vars['robot_position_c'])
        robot_set.append(obj2np(self.kortex.feedback()))

        self.kortex.goContinuous(0.1, 0.0, 0.)
        time.sleep(1.)
        self.kortex.goContinuous()
        time.sleep(.1)
        camera_set.append(self.vars['robot_position_c'])
        robot_set.append(obj2np(self.kortex.feedback()))

        n = 1.
        min_calibration_size = int(self.config["calibration_size"])

        def mv_rob(n, x, y, z):
            self.kortex.goContinuous(x * n, y * n, z * n)
            time.sleep(.5 * n)
            self.kortex.goContinuous()
            time.sleep(.3)
            camera_set.append(self.vars['robot_position_c'])
            robot_set.append(obj2np(self.kortex.feedback()))

        while len(camera_set) < min_calibration_size:
            mv_rob(n, -.15, -.1, -.1)
            mv_rob(n, -.1, .1, 0.)
            mv_rob(n, .15, -.15, 0.)
            mv_rob(n, 0., .15, .1)
            mv_rob(n, .1, .1, 0.)
            mv_rob(n, .12, -.12, .15)
            mv_rob(n, -.12, .12, -.15)
            mv_rob(n, 0., -.2, 0.)
            mv_rob(n, 0., .2, 0.)

            n = n * .8

        self.kortex.goContinuous()
        time.sleep(.3)
        self.kortex.goHome()

        position = obj2np(self.kortex.feedback())
        position.shape = (3, 1)
        A = np.array(robot_set).T
        B = np.array(camera_set).T
        R, t = rigid_transform_3D(A, B)
        print(R.shape, t.shape, position.shape)
        print("Transfered position robot:", ((R @ position) + t) * 1000.)
        print("Position robot:", position)
        print("Position robot camera:", np.array(self.vars['robot_position_c']) * 1000.)
        B2 = ((R @ A) + t)
        error = B2 - B
        error = np.sum(error * error)
        error = np.sqrt(error / len(robot_set))
        print("RMSE:", error)
        print("Transformation:", [R.tolist(), t.tolist()])

        self.transformation = [R, t]

    def loop(self):
        if self.kortex is None:
            raise ConnectionError("RobotController: kortex not initialized before loop.")

        logger.info("RobotController: running control loop")
        while self.vars['shouldRun']:

            # goal = np.array([self.vars['x_goal'], self.vars['y_goal'], self.vars['z_goal']])
            # goal = goal / 1000.

            # TODO: make everything in same units [m, s, m/s...]
            # TODO: next goal is going to be goal + velocity*0.05sec (avg loop time)
            y_velocity_hand = self.vars['y_velocity'][self.vars['window_index']]
            if not np.isnan(self.vars['y_goal']) and not np.isnan(y_velocity_hand):
                print(self.vars['y_goal'], y_velocity_hand)
                if y_velocity_hand < .01:
                    y_velocity_hand = 0
                y_goal = self.vars['y_goal'] + y_velocity_hand*0.045
                robot_position = obj2np(self.kortex.feedback())
                robot_position.shape = (3, 1)
                robot_transformed = ((self.transformation[0] @ robot_position) + self.transformation[1]).tolist()
                # print("y_goal", y_goal, "robot_transformed", robot_transformed, robot_transformed[1][0])
                y_robot = robot_transformed[1][0]
                y_velocity_goal = (y_goal - y_robot)*20.  # = /0.05

                diff = y_goal - y_robot
                if 0.1 > diff > 0.01:
                    y_velocity_goal = y_velocity_goal/2.
                elif diff <= 0.01:
                    y_velocity_goal = diff
                print("velocity goal", y_velocity_goal, "y_hand", y_goal, "y_robot", y_robot, "diff", y_goal - y_robot)
                self.kortex.goContinuous(0., y_velocity_goal, 0.)

            # current = np.array(self.kortex.feedback())
            # # current_offested = current - reference_position  # meters
            # # difference = (goal - current_offested) * 20.
            #
            # pid = PID(1., .1, .05, setpoint=goal[0])
            #
            # timestamps = self.vars['window_timestamps']
            # if 0. not in timestamps:
            #     # Window is full, compute velocity
            #     window_index = self.vars['window_index']
            #     timestamps = self.vars['window_timestamps']
            #     y_window = self.vars['y_window']
            #     y_velocity = np.sum(np.array(self.vars['y_velocity']))
            #
            #     logger.info(f'Velocity: {y_velocity}, {difference[1]}, [{y_velocity + difference[1]}]')
            #     difference[1] = y_velocity + difference[1]
            #
            #
            # else:
            #     # Try to guess velocity?
            #     pass

            # logger.info(f'RobotController: goal = {np.round(goal, 2)}, current = {np.round(current, 2)}')
            # logger.info(f'RobotController: difference = {np.round(difference, 2)}')
            # time.sleep(1)
            # Minimize error distance using velocity control
            # prev_goal = y_goal
            # window.append(time.time() - now)
            # if len(window) > 100:
            #     logger.info(f'Average time between requests: {round(len(window) / sum(window), 2)} Hz')
            #     window = []
            # now = time.time()

    def run(self):
        while self.vars['shouldRun']:
            try:
                logger.info('RobotController: trying to connect to the robot')
                with utilities.DeviceConnection.createTcpConnection({
                    'ip': self.config['ip'],
                    'username': self.config['username'],
                    'password': self.config['password']
                }) as router:
                    with KortexController(router) as kortex:
                        self.kortex = kortex
                        self.init()

                        self.kortex.goContinuous(.2)
                        time.sleep(1.)
                        self.kortex.goContinuous(0.)
                        # for i in range(21):
                        #     if i == 20:
                        #         self.kortex.goContinuous()
                        #         time.sleep(.3)
                        #     robot_position = np.array(kortex.feedback())
                        #     robot_position.shape = (3, 1)
                        #     calculated_position = (self.transformation[0] @ robot_position + self.transformation[1]).T * 1000.
                        #     camera_position = np.array(self.vars["robot_position_c"]) * 1000.
                        #     time.sleep(.3)
                        #     print("Norm:", np.linalg.norm(calculated_position - camera_position), "Calculated:",
                        #           calculated_position.tolist()[0], "Camera", camera_position)
                        # self.kortex.goContinuous(.0)
                        # now = time.time()
                        # window = []

                        self.loop()
            except Exception as e:
                logging.error(f'RobotController: An exception occurred: {e}')


if __name__ == '__main__':
    from support import config

    RobotController(config['RobotController'])

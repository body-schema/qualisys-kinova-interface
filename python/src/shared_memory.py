class SharedMemory(object):
    """SharedMemory is the proxy for shared variables used by SharedMemoryManager. It is not recommended to be used on
    its own.

    Attributes:
        name (string): Name of the variable
        _value (multiprocessing.Value): Internal storage for variable created by given manager"""

    def __init__(self, manager, name, value):
        """Initialize shared variable.

        Args:
            manager (multiprocessing.Manager): Internally used.
            name (string): Name of the variable.
            value (c_type compatible value): initial value, only basic types such as strings, numbers are allowed."""
        self.name = name
        self._value = manager.Value(type(value), value=value, lock=False)
        self._lock = manager.Lock()

    @property
    def value(self):
        """Getter for shared memory value."""
        try:
            with self._lock:
                val = self._value.value
            return val
        except Exception as e:
            raise ValueError(f'SharedMemory: An error occurred when trying to read {self.name}, ({e}).')

    @value.setter
    def value(self, new_value):
        """Setter for shared memory value."""
        with self._lock:
            self._value.value = new_value
        try:
            pass
        except Exception as e:
            raise ValueError(
                f'SharedMemory: An error occurred when trying to set variable {self.name}={new_value}, ({e}).')

    def cleanup(self):
        """Cleanup function before exiting - removing references."""
        with self._lock:
            self._value = None


class SharedMemoryProxy(object):
    """Proxy for accessing shared memory in dictionary.
    Used for writing/reading as object[var_name] = value, object[var_name] or object.keys().

    Makes it easier to write/read into/from shared memory, instead of vars[var_name].value access
    provides direct access and acts as dictionary to the end-user.

    init_var is not intended to be used by other than SharedMemory."""
    def __init__(self, variables={}):
        self._vars = variables

    def keys(self):
        """Return dictionary keys."""
        return self._vars.keys()

    def init_var(self, key, value):
        """Adds a new dictionary key, value pair to internal dictionary.
        Should not be used directly.

        Args:
            key (string): variable name
            value (SharedMemory object): object with contents of variable."""
        self._vars[key] = value

    def __getitem__(self, key):
        """Returns shared memory value.
        Used as object[key] getter.

        Args:
            key (string): variable name"""
        return self._vars[key].value

    def __setitem__(self, key, value):
        """Sets value to shared memory variable named key.
        Used as object[key] = value setter.

        Args:
            key (string): variable name
            value (c_type compatible value): value to be set. Once initialized, the type must stay the same."""
        self._vars[key].value = value

    def __delitem__(self, key):
        """Removes shared variable.
        Used on cleanup.

        Args:
            key (string): variable name"""
        self._vars[key].cleanup()
        del self._vars[key]


class SharedMemoryManager(object):
    """SharedMemoryManager manages multiple shared memory objects across processes.

    Object contains functions to initialize variables. To write/read variables inside a process, pass vars,
    than use vars[var_name] = .../vars[var_name], which takes care of object locking.

    It is done through SharedMemoryProxy, which acts as dictionary. All variables must be initialized before running
    the processes.

    Attributes:
        vars {variable_name: variable_data, ...}: Stores shared variables."""

    def __init__(self, manager, variables=None):
        """Initialize SharedMemoryManager object. Should be used via with SharedMemoryManager(...): to exit correctly.

        Args:
              manager (multiprocessing.Manager): Used internally.
              variables ({var_name: value, ...}): Variables to be initialized."""
        self.__manager = manager
        self.vars = SharedMemoryProxy()

        if variables is not None:
            self.init_vars(variables)

    def init_vars(self, variables):
        """Initializes multiple variables at once.

        Args:
            variables ({var_name: value, ...}): Variables to be initialized."""
        for var in variables.keys():
            self.init_var(var, variables[var])

    def init_var(self, key, value):
        """Initializes variable.

        Args:
            key (string): name of the variable.
            value (c_type compatible value): initial value, only basic types such as strings, numbers are allowed."""
        if key in self.vars.keys():
            raise NameError(f'Trying to initialize variable {key}={value}, that already exists.')
        self.vars.init_var(key, SharedMemory(self.__manager, key, value))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Triggers cleanup function before exiting. Runs gc.collect() internally."""
        for var in list(self.vars.keys()):
            del self.vars[var]


def __modify_values(vars):
    """Sample writing function."""
    while vars["a"] < 1000:
        vars["a"] = vars["a"] + 1
        vars["b"] = vars["b"] + 1
        vars["c"] = "reasdgjoiijecopasd"
        vars["c"] = vars["c"][:vars["a"] % 3]
        vars["d"] = not vars["d"]
        print("adding")


def __read_values(vars):
    """Sample reading function."""
    while vars["a"] < 1000:
        for i in range(1000):
            pass
        print("-----")
        for var in vars.keys():
            print(f'{var} = {vars[var]}')


if __name__ == "__main__":
    from multiprocessing import Manager, Process

    initial_variables = {"a": 1, "b": 3.3, "c": "string", "d": True}
    with Manager() as manager:
        with SharedMemoryManager(manager, initial_variables) as shm:
            p1 = Process(target=__modify_values, args=(shm.vars,))
            p2 = Process(target=__modify_values, args=(shm.vars,))
            p3 = Process(target=__modify_values, args=(shm.vars,))
            p4 = Process(target=__read_values, args=(shm.vars,))

            p1.start()
            p2.start()
            p3.start()
            p4.start()

            p1.join()
            p2.join()
            p3.join()
            p4.join()

import asyncio
import time
from utilities import *

import numpy as np
import qtm
import logging

from rigid_transform_3D import rigid_transform_3D

logger = logging.getLogger(__name__)

class CameraReader(object):
    """CameraReader for reading frames from Qualisys motion capture system. Processes packet by applying transformation.
    """

    def __init__(self, config, shared_variables):
        self.vars = shared_variables
        self.config = config

    def on_packet(self, packet):
        header, markers = packet.get_3d_markers()
        # logger.info(f'Hand: {markers[6]}, Triangle: {markers[1]}, Triangle: {markers[6].y-markers[1].y}')

        #        if markers[6].y < 5:
        #            return
        labels = dict(
            triangle_1=0,
            triangle_2=1,
            triangle_3=2,
            triangle_4=3,
            robot_top=4,
            robot_end=5,
            hand_end=6,
            wrist_left=7,
            wrist_right=8,
        )
        y = float(markers[6].y - markers[1].y - 350)
        # logger.info(x)

        robot_position = obj2np(markers[labels["robot_end"]])/1000.
        hand_position = (obj2np(markers[labels["hand_end"]])/1000.).tolist()[1]

        if not np.isnan(robot_position).all():
            robot_position
            self.vars['robot_position_c'] = robot_position.tolist()

        if not np.isnan(hand_position):
            window_index = self.vars['window_index']

            timestamps = self.vars['window_timestamps']
            timestamps[window_index] = time.time()
            self.vars['window_timestamps'] = timestamps

            y_window = self.vars['y_window']
            y_window[window_index] = hand_position
            self.vars['y_window'] = y_window

            y_velocity = self.vars['y_velocity']
            if self.vars['last_timestamp'] != 0.:
                y_velocity[window_index] = .001 * (hand_position - self.vars['y_goal']) / (
                        time.time() - self.vars['last_timestamp'])
            else:
                y_velocity[window_index] = 0.
            self.vars['y_velocity'] = y_velocity

            self.vars['window_index'] = (self.vars['window_index'] + 1) % 5

            self.vars['last_timestamp'] = time.time()
            self.vars['x_goal'] = 240
            self.vars['y_goal'] = hand_position
            self.vars['z_goal'] = 0.

        # logger.info(f"CameraReader: {packet.timestamp} {self.vars['x_goal']}, {self.vars['y_goal']}, {self.vars['z_goal']}")

    async def setup(self):
        connection = await qtm.connect(self.config['ip'], version=self.config['version'], timeout=5)
        if connection is None:
            raise ConnectionError('CameraReader: Unable to establish connection.')

        logging.info(f'CameraReader: Connected, QTM version: {await connection.qtm_version()}')

        # async with qtm.TakeControl(connection, "password"):

        # # Create new recording
        # await connection.new()
        # await connection.await_event(qtm.QRTEvent.EventConnected)
        #
        # # Start recording
        # await connection.start()
        # await connection.await_event(qtm.QRTEvent.EventCaptureStarted)

        # Start reading frames
        await connection.stream_frames(components=["3d"], on_packet=self.on_packet)

        # Record for 5 seconds
        await asyncio.sleep(120)

        # Stop reading frames
        await connection.stream_frames_stop()

        # # Stop recording frames
        # await connection.stop()
        # await connection.await_event(qtm.QRTEvent.EventCaptureStopped)
        #
        # # Save recording
        # overwrite = True
        # await connection.save(r"test_automatic_recording.qtm", overwrite)
        #
        # # Close file
        # result = await connection.close()
        # if result == b"Closing connection":
        #     await connection.await_event(qtm.QRTEvent.EventConnectionClosed)

        connection.disconnect()

    def run(self):
        asyncio.ensure_future(self.setup())
        asyncio.get_event_loop().run_forever()


if __name__ == '__main__':
    from multiprocessing import Manager
    from shared_memory import SharedMemoryManager
    from support import config

    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    with Manager() as manager:
        with SharedMemoryManager(manager, variables={'x_goal': 0., 'y_goal': 0., 'z_goal': 0.}) as shared_memory:
            cr = CameraReader(config['CameraReader'], shared_memory.vars)
            cr.run()

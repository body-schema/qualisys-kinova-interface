#! /usr/bin/env python3

###
# KINOVA (R) KORTEX (TM)
#
# Copyright (c) 2018 Kinova inc. All rights reserved.
#
# This software may be modified and distributed
# under the terms of the BSD 3-Clause license.
#
# Refer to the LICENSE file for details.
#
# Based on twist_command example:
# https://github.com/Kinovarobotics/kortex/blob/392c312d57912e9645c70cb3e783bdcdb89371bd/api_python/examples/102-Movement_high_level/03-twist_command.py
###

from kortex_api.autogen.client_stubs.BaseClientRpc import BaseClient
from kortex_api.autogen.messages import Base_pb2
import logging
import time

logger = logging.getLogger(__name__)


class KortexController(object):
    """KortexController provides easier way to use KortexAPI for controlling Kinova3 robot.
    Forces robot to home position on start. Recommended to be used with 'with ...' statement for safety."""
    def __init__(self, router=None):
        """Initialisation of robot controller.
        Forces robot to home position on start.

        If router is None, running as simulation mode, actions are logged.

        Args:
            router (RouterClient.RouterClient or None): Router setup for BaseClient, possibly initialised by value from
                utilities DeviceConnection.createTcpConnection"""
        self.__router = router
        if router is None:
            self.__base = None
            logger.warning("KortexController: Router is not defined. Working in simulation mode.")
        else:
            self.__base = BaseClient(router)
        self.goHome(True)

    def goHome(self, initialization=False):
        """Robot goes to its home position.

        Args:
            initialization (bool): Should the gripper go to default position also?"""
        if self.__router is None:
            logger.info("KortexController: Robot not connected, goHome called")
            return True

        # Make sure the arm is in Single Level Servoing mode
        base_servo_mode = Base_pb2.ServoingModeInformation()
        base_servo_mode.servoing_mode = Base_pb2.SINGLE_LEVEL_SERVOING
        self.__base.SetServoingMode(base_servo_mode)

        # Move arm to ready position
        logger.debug('KortexController: Arm moving to safe position.')
        action_type = Base_pb2.RequestedActionType()
        action_type.action_type = Base_pb2.REACH_JOINT_ANGLES
        action_list = self.__base.ReadAllActions(action_type)
        action_handle = None
        for action in action_list.action_list:
            if action.name == "Retract":
                action_handle = action.handle

        if action_handle is None:
            raise RuntimeError("KortexController: Can't reach safe position")

        self.__base.ExecuteActionFromReference(action_handle)
        if initialization:
            time.sleep(5)  # Leave time to action to complete
            self.goGripper(-.1)  # Close gripper
            time.sleep(1)
        return True

    def goContinuous(self, x=0., y=0., z=0., ax=0., ay=0., az=0.):
        """Robot goes into continuous movement by setting velocity for each axis.
        To stop the robot, it is possible to call this without any argument / or use stop().

        Args:
            x, y, z (float): Linear x, y, z-axis velocity (m/s)
            ax, ay, az (float): Angular x, y, z-axis velocity (deg/s)"""
        if self.__router is None:
            logger.info(f'KortexController: Robot not connected, goContinuous called with x,y,z=({x}, {y}, {z}), ax,'
                        f'ay,az=({ax}, {ay}, {az})')
            return

        command = Base_pb2.TwistCommand()

        command.reference_frame = Base_pb2.CARTESIAN_REFERENCE_FRAME_BASE
        command.duration = 0

        twist = command.twist
        twist.linear_x = x
        twist.linear_y = y
        twist.linear_z = z
        twist.angular_x = ax
        twist.angular_y = ay
        twist.angular_z = az

        logger.debug(f'KortexController: goContinuous called with x,y,z=({x}, {y}d, {z}), ax,ay,az=({ax}, {ay}, {az})')
        self.__base.SendTwistCommand(command)

    def goGripper(self, speed=0.):
        """Set speed of the robot gripper.

        Args:
            speed (float between -1. and 1.): speed of the gripper."""
        if self.__router is None:
            logger.info(f'KortexController: Robot not connected, goGripper called with speed={speed}')
            return

        # Create the GripperCommand we will send
        gripper_command = Base_pb2.GripperCommand()
        finger = gripper_command.gripper.finger.add()

        gripper_command.mode = Base_pb2.GRIPPER_SPEED
        finger.value = speed
        logger.debug(f'KortexController: goGripper called with speed={speed}')
        self.__base.SendGripperCommand(gripper_command)

    def feedback(self):
        """Get robots cartesian position x, y, z-axes."""
        if self.__router is None:
            logger.info(f'KortexController: Robot not connected, feedback returns 0,0,0.')
            return 0, 0, 0

        feedback = self.__base.GetMeasuredCartesianPose()
        return feedback.x, feedback.y, feedback.z

    def stop(self):
        """Stop robot movement.
        Internally calls goContinuous without any values."""
        self.goContinuous()

    def __enter__(self):
        """Intended to be used with 'with ...' command for safety stop on exit."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Stops the robot on exit."""
        self.stop()
        logger.info(f'KortexController: Exiting, reason: {exc_type}, {exc_val}, {exc_tb}')


if __name__ == '__main__':
    import time
    import kortex_utilities as utilities
    from support import config

    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    with utilities.DeviceConnection.createTcpConnection({
        'ip': config['RobotController']['ip'],
        'username': config['RobotController']['username'],
        'password': config['RobotController']['password']
    }) as router:
        with KortexController(router) as kortex:
            kortex.goHome()
            logger.info(kortex.feedback())
            # kortex.goGripper(-.1)
            time.sleep(1.)
            kortex.goContinuous(.1, 0.0, 0.)
            time.sleep(1.)
            kortex.goHome()
            time.sleep(4.)
            logger.info(kortex.feedback())

import numpy as np
#from simple_pid import PID


def obj2np(obj):
    """Convert obj.x, obj.y, obj.z to np.array (x,y,z)"""
    if type(obj) == tuple:
        return np.array(obj)
    return np.array([obj.x, obj.y, obj.z])


# class PIDVect(object):
#     def __init__(self, listed_parameters):
#         self.PIDs = []
#         self.size = len(listed_parameters)
#         for i in range(self.size):
#             self.PIDs.append(PID(*listed_parameters[i]))
#
#     def update(self, values):
#         ret = []
#         for i in range(self.size):
#             ret.append(self.PIDs(values[i]))
#         return ret
